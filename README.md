# Forefront

# First-time setup
Run:
```
pipenv install
npm run migrate
```
To sync the local database. You will need to run this command after making any changes to models.

```
pipenv shell
python manage.py createsuperuser
```


## Starting
Run:
```npm start```
And open http://localhost:8000 in your browser.


file:///Users/arama/git/forefront-v1.0/frontend/index.html