from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from projects.views import (HomeView, 
        LabsView, AuthorView, CreateView, LoginView, 
        SignupView, ProfileView, LabView, ProjectView, 
        SearchView, UploadView, LogoutView, MemberAutocompleteView, 
        PreviewView, AuthorCreate, LikesView, GetUniversityListView, 
        signup, activate, account_activation_sent,
        new_lab_users_list, new_lab_user_action, ProjectEditView, 
        # LabUploadView, 
        LabPreviewView, LabEditView, DeleteCommentAjaxView, ProfessorAutocompleteView,
        AuthorAutocompleteView, UniversityAutocompleteView, RemoveSessionsView, search_paginate_view, search_paginate_json_view, search_paginate_projects_view, search_paginate_groups_view, search_paginate_members_view, UserInvitationView
)
# SearchPaginationView
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HomeView.as_view(), name='home'),
    path('labs/', LabsView.as_view(), name='labs'),
    path('author', AuthorCreate.as_view()),
    path('author/<int:pk>', AuthorView.as_view(), name='author'),
    path('create', CreateView.as_view(), name='create'),
    path('lab/<int:pk>', LabView.as_view(), name='lab'),
    path('login/', LoginView.as_view(), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    #path('signup', SignupView.as_view(), name='signup'),
    path('account_activation_sent/', account_activation_sent, name='account_activation_sent'),
    path('activate/<uidb64>/<token>/',activate, name='activate'),
    path('signup/', signup, name='signup'),
    path('profile/', ProfileView.as_view(), name='profile'),
    path('project/<int:pk>/', ProjectView.as_view(), name='project'),
    path('search', SearchView.as_view(), name='search'),
    path('upload/', UploadView.as_view(), name='upload'),
    path('upload/<int:id>/',ProjectEditView.as_view(), name="project-edit"),
    # path('lab/upload/', LabUploadView.as_view(), name='lab_upload'),
    path('lab/preview/', LabPreviewView.as_view(), name='lab_preview'),
    path('lab/edit/<int:pk>/', LabEditView.as_view(), name='lab_edit'),    
    path('preview/', PreviewView.as_view(), name="preview"),
    path('authors-autocomplete', AuthorAutocompleteView.as_view(), name='authors-autocomplete'),
    path('members-autocomplete', MemberAutocompleteView.as_view(), name='members-autocomplete'),
    path('professors-autocomplete', ProfessorAutocompleteView.as_view(), name='professors-autocomplete'),
    path('university-autocomplete', UniversityAutocompleteView.as_view(), name='university-autocomplete'),
    path('likes/', LikesView.as_view(), name='likes'),
    path('university-list/', GetUniversityListView.as_view(), name='university-list'),
    path('new-lab-users-list/', new_lab_users_list, name='new_lab_user'),
    path('new-lab-user/<str:action>/<int:user_id>/', new_lab_user_action, name='new_lab_user_action'),
    path('delete-comments-ajax/', DeleteCommentAjaxView.as_view(), name='delete-comments-ajax'),
    path('remove-sessions/', RemoveSessionsView.as_view(), name='remove-sessions'),
    path('search-paginate/', search_paginate_view, name='search-page'),
    path('search-paginate-json/', search_paginate_json_view, name='search-page-json'),
    path('search-paginate-projects/<str:search_data>/', search_paginate_projects_view, name='search-page-projects'),
    path('search-paginate-groups/<str:search_data>/', search_paginate_groups_view, name='search-page-groups'),    
    path('search-paginate-members/<str:search_data>/', search_paginate_members_view, name='search-page-members'),
    path('invite/', UserInvitationView.as_view(), name='invite'),
    
    # path('save-author/', SaveAuthorView.as_view(), name='saveauthor'),
] 


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)



