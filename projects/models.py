from django.db import models
from django.db.models.fields import CharField, TextField, DateField, EmailField, URLField, IntegerField
from django.db.models import ImageField
from django.contrib.auth.models import User
from django.contrib.staticfiles.templatetags.staticfiles import static

class CustomUser(models.Model):
    USER_TYPES = [
        ('SU', 'Student'),
        ('LA', 'Lab Admin'),
        ('PF', 'Professor'),
    ]

    USER_IS_APPROVED = 1
    USER_IS_NOT_APPROVED = 0

    # Reference to the User model
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)

    # The type of student
    #   SU - Student
    #   LA - Lab Admin
    #   PF - Professor
    type = models.CharField(
        max_length = 2,
        choices=USER_TYPES,
        default='SU',               # Default is Student
    )
    status = models.IntegerField(default=USER_IS_NOT_APPROVED)  # 0 - Not approved
                                                                # 1 - Approved

    # def __str__(self):
    #     return self.user


class Member(models.Model):
    user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, null=True)
    # user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    first_name = CharField(max_length=50)
    last_name = CharField(max_length=50)
    image = ImageField(null=True, blank=True, max_length=255)
    position = TextField(blank=True)
    ideal_role = TextField(blank=True)
    education = TextField(null=True, blank=True)       # no use for upload page
    website = URLField(blank=True)
    # email = EmailField(blank=True)
    main_lab = models.ForeignKey("Lab", on_delete=models.SET_NULL, null=True)   # no use for upload page
    university = models.ForeignKey("University", on_delete=models.SET_NULL, null=True)
    def __str__(self):
        return self.first_name + " " + self.last_name

    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        return static('projects/images/default.png')



class University(models.Model):
    name = CharField(max_length=50, null=True)
    class Meta:
        verbose_name_plural = "universities"
    def __str__(self):
        return self.name

class MemberQualification(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    university = models.ForeignKey(University, on_delete=models.CASCADE)
    degree = models.CharField(max_length=50)
    year = models.IntegerField()

class MemberWorkExperience(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    company = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    yearfrom = models.IntegerField()
    yearto = models.IntegerField()


class Project(models.Model):
    image = ImageField(null=True, max_length=255)   # banner image
    excerpt = TextField(null=True)
    body = TextField()
    name = CharField(max_length=200)
    authors = models.ManyToManyField(Member)
    likes = IntegerField(default=0)
    lab = models.ForeignKey("Lab", on_delete=models.SET_NULL, null=True)
    image1 = ImageField(null=True, max_length=255, blank=True)
    image2 = ImageField(null=True, max_length=255, blank=True)
    image3 = ImageField(null=True, max_length=255, blank=True)
    created_date = models.DateField(auto_now_add=True)
    nature_contribution = models.CharField(null=True,max_length=200)    # disabled this field for now.
    def __str__(self):
        return self.name
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        return static('projects/images/default.png')

    def image1_url(self):
        if self.image1 and hasattr(self.image1, 'url'):
            return self.image1.url
        return None

    def image2_url(self):
        if self.image2 and hasattr(self.image2, 'url'):
            return self.image2.url
        return None

    def image3_url(self):
        if self.image3 and hasattr(self.image3, 'url'):
            return self.image3.url
        return None

class MemberProjectComment(models.Model):
    member = models.ForeignKey(Member, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    comment = TextField()
    is_owner = IntegerField(default=0, null=True)


class Lab(models.Model):
    owner   = models.OneToOneField(CustomUser, on_delete=models.CASCADE, related_name='labs', null=True, blank=True)
    name = CharField(max_length=200)
    image = ImageField(null=True)
    professors = models.ManyToManyField(Member, related_name='professors')
    members = models.ManyToManyField(Member, related_name='members')
    excerpt = TextField(blank=True, null=True)
    university = models.ForeignKey("University", on_delete=models.SET_NULL, null=True)
    major_contributions = TextField(blank=True, null=True)
    sources_of_funding = TextField(blank=True, null=True)
    def __str__(self):
        return f"{self.name} ({self.university})"
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        return static('projects/images/7cb07f_db9a29e4c52744729b8cb99f1a87dc05-mv2.jpg')

class LabResearchTopic(models.Model):
    name = CharField(max_length=200)
    image = ImageField(null=True)
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    excerpt = TextField(blank=True, null=True)
    def __str__(self):
        return self.name
    def image_url(self):
        if self.image and hasattr(self.image, 'url'):
            return self.image.url
        return static('projects/images/7cb07f_db9a29e4c52744729b8cb99f1a87dc05-mv2.jpg')

class LabPublication(models.Model):
    date = DateField(auto_now_add=True)
    citation = TextField()
    lab = models.ForeignKey(Lab, on_delete=models.CASCADE)
    class Meta:
        ordering = ['-date']
    def __str__(self):
        return f"{self.date}: {self.lab}"


class Userlikes(models.Model):
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE, null=True)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    like = IntegerField(default=0)
