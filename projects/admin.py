from django.contrib import admin
from .models import Member, Project, Lab, University, LabResearchTopic, LabPublication, CustomUser, MemberProjectComment

# Register your models here.
admin.site.register(Project)
admin.site.register(Member)
admin.site.register(Lab)
admin.site.register(University)
admin.site.register(LabResearchTopic)
admin.site.register(LabPublication)

@admin.register(CustomUser)
class CustomUserAdmin(admin.ModelAdmin):
    list_display = ['user', 'type', 'status']
    list_editable = ['status']

@admin.register(MemberProjectComment)
class MemberProjectCommentAdmin(admin.ModelAdmin):
    list_display = ['comment']
    search_fields = ['comment']


# admin.site.register(CustomUser)
