function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
function dataURLtoFile(dataurl, filename) {
 
    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});
}
function check_field_validation(input_field, error_msg) {
    if(!isNaN(input_field)) {
        $('.error_area').append('<p class="error_field">'+error_msg+'</p>')
        return false;
    }
    return true;
}
function check_profile_image_validation(input_field, error_msg) {
    split_string = input_field.split("/")
    if(split_string[split_string.length-2] == 'images' && split_string[split_string.length-3] == 'projects') {
        $('.error_area').append('<p class="error_field">'+error_msg+'</p>')
        return false;
    }
    return true;
}
function check_select_field_validation(input_field, error_msg) {
    if(!input_field) {
        $('.error_area').append('<p class="error_field">'+error_msg+'</p>')
        return false;
    }
    return true;
}
function upload_page_validation(input_field, error_msg) {
    if(!isNaN(input_field)) {
        alert(error_msg);
        return false;
    }
    return true;
}
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
        $('.error_area').append('<p class="error_field">* Email is not valid.</p>')
    }
    return regex.test(email);
  }

function university_list() {
    $.ajax({
        url: '/university-list/',
        method : 'GET',
        dataType:'json',
        success: function(result) {
            var uni_arr = [];
            json_str = JSON.stringify(result)
            jQuery("#university_list").attr("data", json_str)
            uni_arr.push('<option value="">Select University</option>');
            for (var uni of result) {
                uni_arr.push('<option value="'+uni.id+'">'+uni.name+'</option>');
            }
        }
    });
}

function get_edu_records_length(increment=false, decrement=false) {
    
    if($("#edu_records_length").length > 0) {
        if(increment && decrement) {
            // page load
            console.log('coming here')
        }
        else if(increment && !decrement) {
            // trigger when clicking on + sign
            $("#edu_records_length").val(parseInt($("#edu_records_length").val())+1)
        }
        else {
            // trigger when clicking on - sign
            if(!increment && decrement) {
                $("#edu_records_length").val(parseInt($("#edu_records_length").val())-1)
            }

        }
        
    }

    return false;
}

function get_exp_records_length(increment=false, decrement=false) {
    
    if($("#exp_records_length").length > 0) {
        if(increment && decrement) {
            // page load
            // $("#exp_records_length").val($(".exp-list").length)
            console.log('coming here')
        }
        else if(increment && !decrement) {
            // trigger when clicking on + sign
            $("#exp_records_length").val(parseInt($("#exp_records_length").val())+1)
        }
        else {
            if(!increment && decrement) {
                // trigger when clicking on - sign
                $("#exp_records_length").val(parseInt($("#exp_records_length").val())-1)
            }
        }
        
    }

    return false;
}

function set_img(src, target, callback) {
    var reader = new FileReader();
    reader.addEventListener('load', function (ev) {
        $(target).attr('src', ev.target.result);
        if (callback) {
            callback(ev.target.result);
        }
    });
    reader.readAsDataURL(src);
}

function words_limit(text_data, text_length) {
    splitted_text = text_data.split(" ")
    if(splitted_text.length > text_length) {
        return false;
    }
    return true;
}
function get_filtered_authors() {
    authors_arr = []
    already_exist_authors = []
    is_author_exist = 0
    // if existing authors exist, we need to assign them to an array variable.
    if(jQuery("#authors_area .single_author").length > 0) {
        jQuery("#authors_area .single_author").each(function(index, elem) {
            id = jQuery(this).find('span.author-remove').attr('author_id')
            first_name = jQuery(this).find('span.author-remove').attr('first_name')
            last_name = jQuery(this).find('span.author-remove').attr('last_name')
            comment = jQuery(this).find('span.author-remove').attr('comment')
            image = jQuery(this).find('span.author-remove').attr('image')
            position = jQuery(this).find('span.author-remove').attr('position')
            already_exist_authors.push({id: id, first_name: first_name, last_name: last_name, comment: comment, image: image, position: position})
            
        });
    }


    if(jQuery(".select2-selection--multiple ul li.select2-selection__choice span").length > 0) {
        jQuery(".select2-selection--multiple ul li.select2-selection__choice span").find('input').each(function() {
            
            if(already_exist_authors.length > 0) {
                authors_arr = already_exist_authors    // assign already existing authors to authors_arr
                for(author of already_exist_authors) {
                    if(author['id'] == jQuery(this).attr('id')) {
                        is_author_exist = 1
                        break
                    }
                }
                if(is_author_exist == 0) {
                    authors_arr.push(JSON.parse(jQuery(this).attr('data')))      
                }
            }
            else {
                authors_arr.push(JSON.parse(jQuery(this).attr('data')))
            }    
        });
    }
    else {
        authors_arr = already_exist_authors
    }    
    return authors_arr;
}


  

$(document).ready(function () {
    
    const img_type = ['banner', 'upm-img1', 'upm-img2', 'upm-img3'];
    var file_names = {};
    base_url = window.location.origin

    // search page.
    if((window.location.pathname == "/search")) {
        console.log('coming in search')
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        form_data = {'start':0, 'search[value]': 'test'}
        url_redirect = window.location.origin+'/search-paginate/'
        jQuery.ajax({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            url:url_redirect,
            method: 'POST',
            data: form_data,
            success: function(result) {
                console.log(result)
                
            }
        })
    }



    // if(window.location.pathname == "/lab/preview/") {
    //     if(jQuery("#labTopicSliderCarousel .carousel-inner")) {
    //         jQuery("#labTopicSliderCarousel .carousel-inner").find('.active').not(":eq(0)").removeClass('active')
    //     }
    // }

    // upload btn clicked on right sidebar. when it is clicked we will be removing all local storages and server sessions.
    jQuery("#forefrontSidebarContent").on("click", "#upload_btn", function() {
        localStorage.clear()
        jQuery.ajax({
            url:base_url+'/remove-sessions/',
			dataType: 'json',
			success: function(data) {
                console.log(data.message)
                
			}
		});        
    });

    // if(window.location.pathname == )
    splitted_str = window.location.pathname.split("/")


    // back button on preview page
    jQuery(".container-fluid").on("click", "#back_btn",function() {
        if(localStorage.getItem("project_id")) {
            project_id = localStorage.getItem("project_id")
            window.location.href = window.location.origin+"/upload/"+project_id+"/"
        }
        else {
            window.location.href = window.location.origin+"/upload/"
        }
        
        return false;
    });

    // to check whether user is created as a member or not. if he is not created as a member, then redirect him to his profile page where he can fill his details.
    
    if((window.location.pathname == "/upload/") || (splitted_str[splitted_str.length - 3] == 'upload' && splitted_str[splitted_str.length - 2] > 0)) {

        var input = document.getElementsByClassName('select2-selection--multiple');
        // jQuery('.select2-selection--multiple').keyup(function (e) {
            console.log('coming in select')
        // autocomplete feature fix.
        input[0].onkeydown = function() {
            var key = event.keyCode || event.charCode;
            // console.log(key)
            // console.log(typeof key)
            // backspace key 8
            if(key == 8) {

                search_str = jQuery(".select2-search__field").val()

                // if searched string is a full user detailed string, then we will compare some parts of the string, if all of them are true then remove that same whole of string. if anyone of them doesn't match, then we will remove string by one character at a time.
                if(search_str.search("input type=") > 0 && search_str.search("class=") > 0 && search_str.search("id=") > 0 && search_str.search("data=") > 0) {
                    jQuery(".select2-search__field").val('')
                }
                else {
                    new_str = search_str.substring(0, search_str.length-1)
                    jQuery(".select2-search__field").val(new_str)
                }
            return false;
            }   
        };




        // remove extra data when pressing backspace
        console.log('coming in author js file')

        

        // if edit project page
        if(splitted_str[splitted_str.length - 3] == 'upload' && splitted_str[splitted_str.length - 2] > 0) {
            project_id = splitted_str[splitted_str.length - 2]
            if(localStorage.getItem("is_preview") && localStorage.getItem("project_id")) {
                // return false;
            // we set is_preview = 1 in localstorage when edit page redirects to preview page, so, we can show authors and comments based on is_preview on present edit page.
            
                if(localStorage.getItem("is_preview") == 1) {
                    
                    // we set project_id in localstorage when edit page redirects to preview page to retain the project id of page then it will come back to edit page from preview page.

                    // when user hit edit page url from address bar, then we check whether localstorage project id matches with the project id of url or not, if it matches, then we dont need to do anything, if it doesn't match, then we need to remove all localstorage variables and all server side sessions corresponding to present project id.
                    if(localStorage.getItem("project_id") == project_id) {
                        // continue
                        jQuery("#authors_area").eq(0).parent().parent().parent().remove();
                        comments_arr = JSON.parse(localStorage.getItem("comments_arr"))
                        if(localStorage.getItem('authors_arr')) {
                            authors_arr = JSON.parse(localStorage.getItem('authors_arr'))
                            if(authors_arr.length > 0) {
                                authors_string = '<div class="author-container"><div class="author-inner"><div class="col-xs-12 align-center"><div id="authors_area" class="col-xs-10 col-xs-offset-2"><ul>';
        
                                for(author of authors_arr) {
                                    member_comment = null
                                    for(single_comment of comments_arr) {
                                        if(single_comment.project_member == author.id) {
                                            member_comment = single_comment.comment
                                            break
                                        }
                                    }    
                                    authors_string += '<li class="single_author"><p style="color: #EECE33;"><span class="author-remove glyphicon glyphicon-remove" project_id="'+project_id+'" author_id="'+author.id+'" first_name="'+author.first_name+'" last_name="'+author.last_name+'" image="'+author.image+'" comment="'+member_comment+'" position="'+author.position+'" style="color: #FED501;font-size: .8em; margin-right: 10px;"></span>'+author.first_name+' '+author.last_name+'</p></li>'
                                }
                                authors_string += '</ul></div></div></div></div>'
                                jQuery(".author-row").prepend(authors_string)
                            }
                        }                                    
                    }
                    else {
                        return false;
                    }
                    var cropping_values = JSON.parse(localStorage.getItem("cropping_values"));
                    for( let c of cropping_values){
                        var image = c["image"];
                        $(`#x_${image}`).val(c['x']);
                        $(`#y_${image}`).val(c['y']);
                        $(`#w_${image}`).val(c['w']);
                        $(`#h_${image}`).val(c['h']);
                    }

                    // setting previous image file name with extensions
                    file_names = JSON.parse(localStorage.getItem("file_names"));
                }                
            }


        }

        // if new upload project page
        if(localStorage.getItem("is_preview") == 1) {
            $("#project_title").text(localStorage.getItem("project_title"));
            $("#excerpt").text(localStorage.getItem("excerpt"));
            // $("#banner-image").attr("src",localStorage.getItem("banner_photo"));
            // $(".img-preview-banner img").attr("src",localStorage.getItem("banner_photo"));
            // $(".img-preview-banner").append(localStorage.getItem("banner_photo"))
            // $(".img-preview-banner").children().attr("src", localStorage.getItem('banner_photo'))
            // console.log('ttr')
            // console.log(localStorage.getItem('banner_photo'))
            if(localStorage.getItem('banner_photo')) {
                $("#parent_banner").children().attr("src", localStorage.getItem('banner_photo'))
            }
            
            // $(".img-preview-banner").children().attr("style", localStorage.getItem('banner_photo_style'))
            
            // $(".img-preview-banner img").css({display:'inline-block'});
            $("#upm-img1").attr("src", localStorage.getItem('upm-img1'))
            $("#upm-img1").css({display:'inline-block'});
            

            $("#upm-img1").attr("style", localStorage.getItem('upm-img1-cssText'))
            $("#upm-img2").attr("src", localStorage.getItem('upm-img2'))
            $("#upm-img2").css({display:'inline-block'});
            $("#upm-img2").attr("style", localStorage.getItem('upm-img2-cssText'))
            $("#upm-img3").attr("src", localStorage.getItem('upm-img3'))
            $("#upm-img3").css({display:'inline-block'});
            $("#upm-img3").attr("style", localStorage.getItem('upm-img3-cssText'))
            $("#full_description").text(localStorage.getItem("full_description"))
            // setting values in hidden input fields for image cropping 
            var cropping_values = JSON.parse(localStorage.getItem("cropping_values"));
            for( let c of cropping_values){
                var image = c["image"];
                $(`#x_${image}`).val(c['x']);
                $(`#y_${image}`).val(c['y']);
                $(`#w_${image}`).val(c['w']);
                $(`#h_${image}`).val(c['h']);
            }

            // setting previous image file name with extensions
            file_names = JSON.parse(localStorage.getItem("file_names"));            
            // show authors only in case of upload new project page but not in edit project page.
            if(window.location.pathname == "/upload/") {
                comments_arr = JSON.parse(localStorage.getItem("comments_arr"))
                if(localStorage.getItem('authors_arr')) {
                    authors_arr = JSON.parse(localStorage.getItem('authors_arr'))
                    if(authors_arr.length > 0) {

                        authors_string = '<div class="author-container"><div class="author-inner"><div class="col-xs-12 align-center"><div class="col-xs-10 col-xs-offset-2" id="authors_area"><ul>';

                        for(author of authors_arr) {
                            member_comment = null
                            for(single_comment of comments_arr) {
                                if(single_comment.project_member == author.id) {
                                    member_comment = single_comment.comment
                                    break
                                }
                            }    
                            authors_string += '<li class="single_author"><p style="color: #FED501;"><span class="author-remove glyphicon glyphicon-remove" project_id="" author_id="'+author.id+'" first_name="'+author.first_name+'" last_name="'+author.last_name+'" image="'+author.image+'" comment="'+member_comment+'" position="'+author.position+'" style="color: #FED501;font-size: .8em; margin-right: 10px;"></span>'+author.first_name+' '+author.last_name+'</p></li>'
                        }
                        authors_string += '</ul></div></div></div></div>'
                        jQuery(".author-row").prepend(authors_string)
                    }
                }
            }


        }
        if(window.location.pathname == "/upload/") {
            if(jQuery("input[name='login_owner_id']").length == 0) {
                // disable outer click
                jQuery('#member_alert').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                jQuery('#member_alert').modal('show');
                jQuery('body').css({'padding-right': 0});   // remove right side garbage area            
            }
        }

    }

    jQuery('#member_alert').on("click",'.profile_redirect_btn', function () {
        window.location.href = window.location.origin+'/profile'
        return false;
    })

    
    jQuery('.edit_project_area').on("click",'.edit_text', function () {
        edit_link = jQuery(this).attr('href')
        localStorage.clear()
        jQuery.ajax({
            url:base_url+'/remove-sessions/',
			dataType: 'json',
			success: function(data) {
                console.log(data.message)
                window.location.href = window.location.origin+edit_link

                return false;
			}

		});
        
        return false;
    })
    var edu_new = $("#edu-new").html();
    var exp_new = $("#exp-new").html();
    var images = { "upm-img1": [], "upm-img2": [], "upm-img3": [] , "banner": []};
    var files = { "upm-img1": "same", "upm-img2": "same", "upm-img3": "same" , "banner": "same"};
    $("#edu-new,#exp-new").remove();


    $('.select2-search__field').each(function(){
        $(this).keydown(function(e){
            if(e.keyCode == 8){
                e.preventDefault();
                e.stopPropagation(); 
                this.value = this.value.substring(0, this.value.length - 1);
                $(this).parent().prev().remove();
                $(this).parent().parent().parent().parent().parent().prev().children().last().remove();
            };
        });
    });
    
    // real-time word count validations
    $('#excerpt').on('keyup',function(e){
        var n = $(this).text().trim().split(' ').length;
        if(e.keyCode == 8){

        }else if(n>100){
            alert('Words length is not allowed more than 100 for excerpt text field.');
            $(this).text($('#excerpt').text().trim().split(' ').slice(0, 100).join(' '));
            n = $(this).text().trim().split(' ').length;
        }
        $(this).addClass('changed').attr('data-content',100-n);
    });

    $('#full_description').on('keyup',function(e){
        var n = $(this).text().trim().split(' ').length;
        if(e.keyCode == 8){

        }else if(n>500){
            alert('Words length is not allowed more than 500 for brief description text field.');
            $(this).text($('#full_description').text().trim().split(' ').slice(0, 500).join(' '));
            n = $(this).text().trim().split(' ').length;
        }
        $(this).addClass('changed').attr('data-content',500-n);
    });

    $("textarea[name='ideal_role']").on('keyup',function(e){
        var n = $(this).val().trim().split(' ').length;
        if(e.keyCode == 8){

        }else if(n>140 && n<=150){
            $('#ideal_role_counter').css('color', '#f31313');
        }else if(n>150){
            alert('Words length is not allowed more than 150 for Ideal Role text field.');
            $(this).val($("textarea[name='ideal_role']").val().trim().split(' ').slice(0, 150).join(' '));
            n = $(this).val().trim().split(' ').length;
        }
        $('#ideal_role_counter').text(150-n);

    });

    // remove students from edit lab page
    $(".student_detail").on("click", ".student-remove", function () {
        current_elem = jQuery(this)
        current_elem.parents().eq(0).remove()   // remove from DOM
    })

    $(".professor_detail").on("click", ".student-remove", function () {
        current_elem = jQuery(this)
        current_elem.parents().eq(0).remove()   // remove from DOM
    })

    // remove authors on edit project page.
    $("#authors_area").on("click", ".author-remove", function () {
        member_id = jQuery(this).attr('author_id')
        project_id = jQuery(this).attr('project_id')
        current_elem = jQuery(this)
        data = {"member_id": member_id, "project_id": project_id}
        jQuery.ajax({
            url:base_url+'/delete-comments-ajax/',
            data: data,
			dataType: 'json',
			success: function(data) {
                console.log(data.message)
                if(data.is_user_login == 1) {
                    alert(data.message)
                }
                else {
                    current_elem.parents().eq(1).remove();
                }

                return false;
			}

		});
    });


    // clicking + sign on education field of author profile page
    $("#edu-add-btn").click(function () {
        var add = true;
        var uni_arr = [];
        var uni_list = $("#university_list").attr('data')
        var parsed_data = JSON.parse(uni_list);
        for (var uni of parsed_data) {
            uni_arr.push('<option value="'+uni.id+'">'+uni.name+'</option>');
            
        }

        
        
        $(".edu-list .edu-name").each(function () {
            if ($(this).val() === "") {
                this.focus();
                add = false;
                return false;
            }
        });
        if (add === true) {
            $(".edu-list").append(edu_new);
            // univerity dropdown field
            row_index = parseInt($("#edu_records_length").val())
            $(".row.edu-row.form-group").last().children().first().children().attr("name","uni"+row_index+"_0")
            $(".row.edu-row.form-group").last().children().last().children().first().children().children().eq(0).attr("name","uni"+row_index+"_1")
            $(".row.edu-row.form-group").last().children().last().children().first().children().children().eq(1).attr("name","uni"+row_index+"_2")
            get_edu_records_length(true, false);    // update hidden field
        }
        
    });

    // clicking - sign on education field of author profile page
    $(".edu-list").on("click", ".item-remove", function () {
        $(this).parents(".edu-row").remove();
    });

    // clicking + sign on work experience field of author profile page
    $("#exp-add-btn").click(function () {
        var add = true;
        $(".exp-list .company-input").each(function () {
            if ($(this).val() === "") {
                this.focus();
                add = false;
                return false
            }
        });
        if (add === true) {
            $(".exp-list").append(exp_new);
            row_index = parseInt($("#exp_records_length").val())
            $(".row.exp-row.form-group").last().children().eq(0).children().eq(0).attr("name","exp"+row_index+"_0")
            $(".row.exp-row.form-group").last().children().eq(1).children().eq(0).attr("name","exp"+row_index+"_1")
            $(".row.exp-row.form-group").last().children().eq(2).children().children().eq(0).children().first().attr("name","exp"+row_index+"_2")
            $(".row.exp-row.form-group").last().children().eq(2).children().children().eq(1).children().first().attr("name","exp"+row_index+"_3")
            get_exp_records_length(true, false) // update hidden field
        }
        
    });

    // clicking - sign on work experience field of author profile page
    $(".exp-list").on("click", ".item-remove", function () {
        $(this).parents(".exp-row").remove();
    });

    // when click on submit button on profile page
    $("#save-profile").click(function() {
        jQuery(".error_area").empty()
        qual_status = 0
        exp_status = 0
        // validationn checks
        first_name_check = check_field_validation($("input[name='first_name']").val(), '* First Name field is empty.')
        last_name_check = check_field_validation($("input[name='last_name']").val(), '* Last Name field is empty.')
        upload_img_check = check_profile_image_validation($('#author-img-view').attr('src'), '* Please upload image.')
        position_check = check_field_validation($("input[name='position']").val(), '* Position field is empty.')
        website_check =check_field_validation($("input[name='website']").val(), '* Website field is empty.')
        institute_check = check_select_field_validation($("select[name='institute']").val(), '* Institute field is empty.')
        ideal_role_check = check_field_validation($("textarea[name='ideal_role']").val(), '* Ideal role field is empty.')
        text_limit = words_limit($("textarea[name='ideal_role']").val().trim(), 150);
        if (!text_limit) {
            alert('Words length is not allowed more than 150 for Ideal role text field.');
            return false;
        }

        if(!first_name_check || !last_name_check || !upload_img_check || !position_check || !website_check || !ideal_role_check || !institute_check) {
            return false;
        }        

        // validation for empty qualification fields
        if($(".edu-list .edu-row").length == 0) {
            $('.error_area').append('<p class="error_field">* Atleast one qualification field is allowed.</p>')
            return false;
        }

        // validation for qualification fields
        $(".edu-list .edu-row").each(function() {
            if($(this).find(".edu-name").val() == '' || $(this).find(".degree").val() == '' || $(this).find(".year").val() == '') {
                qual_status = 1
                $('.error_area').append('<p class="error_field">* Please check qualification fields.</p>')
                return false;                
            }
        });
        if(qual_status == 1) {
            return false;
        }
    })



    var win = "";
    // clicking on preview button on upload page.
    $("#preview-btn").click(function(){
        var cropping_values = [];
        authors_arr = []
        is_author_exist = 0
        already_exist_authors = [] // already existing authors coming on edit page.        
        var form_data = new FormData();

        

        // input fields
        if(jQuery("input[name='is_edit']").length == 0) {
            banner_img_check = upload_page_validation($("#parent_banner").attr("src"), 'Please upload a banner photo to continue.')
            if(!banner_img_check) {
                return false;
            }
        }

        project_title_check = upload_page_validation($("#project_title").text().trim(), 'Project title field is empty.')
        if(!project_title_check) {
            return false;
        }
        
        excerpt_check = upload_page_validation($("#excerpt").text().trim(), 'Excerpt field is empty.')
        if(!excerpt_check) {
            return false;
        }

        full_description_check = upload_page_validation($("#full_description").text().trim(), 'Full description field is empty.')
        if(!full_description_check) {
            return false;
        }

        text_limit = words_limit($("#excerpt").text().trim(), 100)
        if(!text_limit) {
            alert('Words length is not allowed more than 5 for excerpt text field.');
            return false;
        }

        text_limit = words_limit($("#full_description").text().trim(), 500)
        if(!text_limit) {
            alert('Words length is not allowed more than 500 for brief description text field.');
            return false;
        }


        // if edit page exists.
        if(jQuery("input[name='is_edit']").length > 0) {
            if(jQuery("#authors_area .single_author").length > 0) {
                if(jQuery(".select2-selection--multiple ul li.select2-selection__choice span").find('input').length == 0 && jQuery("#authors_area .single_author").length == 0) {
                    alert("Please select atleast one author");
                    return false;
                }
            }
        }
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        localStorage.setItem("project_title", $("#project_title").text().trim());
        localStorage.setItem("excerpt", $("#excerpt").text().trim());
        localStorage.setItem("project_date", $("#project_date").text().trim());
        if($("#parent_banner").children().attr("src") === ""){
            console.log('if cond')
            var bg = $(".img-preview-banner").css('background-image');
            bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");
            localStorage.setItem('banner_photo', bg);
        }else{
            console.log('else cond')
            localStorage.setItem('banner_photo', $("#parent_banner").children().attr("src"));
            // localStorage.setItem('banner_photo_style', $(".img-preview-banner").children().attr("style"));
            // localStorage.setItem('banner-cssText', $(".img-preview-banner").children()[0].style.cssText);
        }
        localStorage.setItem('upm-img1', $(".img-preview-upm-img1").children().attr("src"));
        localStorage.setItem('upm-img2', $(".img-preview-upm-img2").children().attr("src"));
        localStorage.setItem('upm-img3', $(".img-preview-upm-img3").children().attr("src"));
        localStorage.setItem("full_description", $("#full_description").text().trim());


        localStorage.setItem("banner-height", $(".project-banner-img").css("height"));
        
        localStorage.setItem('upm-img1-cssText', $(".img-preview-upm-img1").children()[0].style.cssText);
        localStorage.setItem('upm-img2-cssText', $(".img-preview-upm-img2").children()[0].style.cssText);
        localStorage.setItem('upm-img3-cssText', $(".img-preview-upm-img3").children()[0].style.cssText);
        
        // setting cropping values
        for( let k of img_type){
            var x = $(`#x_${k}`).val();
            var y = $(`#y_${k}`).val();
            var w = $(`#w_${k}`).val();
            var h = $(`#h_${k}`).val();
            cropping_values.push({
                'image': k,
                'x': $(`#x_${k}`).val(),
                'y': $(`#y_${k}`).val(),
                'w': $(`#w_${k}`).val(),
                'h': $(`#h_${k}`).val(),
            })
        }

        localStorage.setItem("cropping_values",JSON.stringify(cropping_values));
        console.log('file_names')
        console.log(file_names)
        localStorage.setItem("file_names", JSON.stringify(file_names));
        
        authors_arr = get_filtered_authors()
        // console.log('authors_arr')
        // console.log(authors_arr)
        // return false;

        // push login member into authors_arr array variable
        if(jQuery("#authors_area .single_author").length == 0) {
            authors_arr.push(JSON.parse(jQuery("input[name='login_member_info']").val()))  
        }

        
        localStorage.setItem('authors_arr', JSON.stringify(authors_arr)); 
        form_data.append("post_type",'author_save_temp')
        is_preview_on_edit = 0
        previewed_project_id = splitted_str[splitted_str.length - 2]
        if(jQuery("input[name='is_edit']").length == 1) {
            url_redirect = '/upload/'+jQuery("input[name='is_edit']").val() + '/'
            is_preview_on_edit = 1
            form_data.append("is_preview_on_edit", is_preview_on_edit) // it will be implemented only in case of preview of during project edit.
            form_data.append("previewed_project_id", previewed_project_id)
        }
        else {
            url_redirect = '/upload/'
        }
        
        form_data.append("authors_arr", JSON.stringify(authors_arr))
        jQuery.ajax({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            url:url_redirect,
            method: 'POST',
            data: form_data,
            processData: false,
            contentType: false,
            async:false,
            success: function(result) {
                if(result.authors_exist.length > 0) {
                    var newline = "\r\n";
                    var message = 'Please enter contributions for the following authors:';
                    for(i of result.authors_exist) {
                        message += newline;
                        message += '* '+i.first_name+' '+i.last_name;
                    }
                    alert(message)
                    return false;
                }
                else {
                    localStorage.setItem("comments_arr",result.comments_arr)
                    if(jQuery("input[name='is_edit']").length == 1) {
                        localStorage.setItem("project_id",splitted_str[splitted_str.length - 2])
                    }
                    else {
                        if(localStorage.getItem("project_id")) {
                            localStorage.removeItem('project_id');
                        }
                    }
                    localStorage.setItem("is_preview",1)
                    
                    if (win == "" || win.closed == true) {
                        win = window.open('/preview', '_self');
                    } else {
                        win.location.reload();
                        win.focus();
                    }
                }
            }
        })

        return false;
    });

    // clicking on publish button of project page
    $("#publish-btn").click(function() {
        var is_author_exist = 0
        var form_data = new FormData();
        authors_arr = [];
        already_exist_authors = [] // already existing authors coming on edit page.
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

        // input fields
        if(jQuery("input[name='is_edit']").length == 0) {
            banner_img_check = upload_page_validation($("#parent_banner").children().attr("src"), 'Please upload a banner photo to continue.')
            if(!banner_img_check) {
                return false;
            }
        }

        project_title_check = upload_page_validation($("#project_title").text().trim(), 'Project title field is empty.')
        if(!project_title_check) {
            return false;
        }
        
        excerpt_check = upload_page_validation($("#excerpt").text().trim(), 'Excerpt field is empty.')
        if(!excerpt_check) {
            return false;
        }

        full_description_check = upload_page_validation($("#full_description").text().trim(), 'Full description field is empty.')
        if(!full_description_check) {
            return false;
        }

        text_limit = words_limit($("#excerpt").text().trim(), 100)
        if(!text_limit) {
            alert('Words length is not allowed more than 5 for excerpt text field.');
            return false;
        }

        text_limit = words_limit($("#full_description").text().trim(), 500)
        if(!text_limit) {
            alert('Words length is not allowed more than 500 for brief description text field.');
            return false;
        }


        // atleast one author should be selected from dropdown.
        // if edit page exists
        if(jQuery("#authors_area .single_author").length > 0) {
            if(jQuery(".select2-selection--multiple ul li.select2-selection__choice span").find('input').length == 0 && jQuery("#authors_area .single_author").length == 0) {
                alert("Please select atleast one author");
                return false;
            }
        }
        authors_arr = get_filtered_authors()

        // push login member into authors_arr array variable
        if(jQuery("#authors_area .single_author").length == 0) {
            authors_arr.push(JSON.parse(jQuery("input[name='login_member_info']").val()))  
        }


        console.log('files["banner"]')
        console.log(files["banner"])
        var url = window.location.pathname;

        // ajax to check comments for authors
        form_data.append("post_type",'author_save_temp')
        
        form_data.append("authors_arr", JSON.stringify(authors_arr))
        // ajax to check validation for comments of authors
        jQuery.ajax({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            url: url,
            method: 'POST',
            data: form_data,
            processData: false,
            contentType: false,
            async:false,
            success: function(result) {
                if(result.authors_exist.length > 0) {
                    var newline = "\r\n";
                    var message = 'Please enter contributions for the following authors:';
                    for(i of result.authors_exist) {
                        message += newline;
                        message += '* '+i.first_name+' '+i.last_name;
                    }
                    alert(message)
                    return false;
                }
                else {
                    form_data.append("authors_arr", JSON.stringify(authors_arr))
                    if(parseInt(localStorage.getItem("is_preview")) !== 1){
                        form_data.append("image", files["banner"]);
                        //form_data.append("image", $("#img-final-banner").attr("src"));
                        form_data.append("image1", files["upm-img1"] );
                        form_data.append("image2", files["upm-img2"] );
                        form_data.append("image3", files["upm-img3"] );
                    }else{
                        var filenames = JSON.parse(localStorage.getItem("file_names"));
                        if(localStorage.getItem("banner_photo") !== "" && localStorage.getItem("banner_photo").indexOf("https://") === -1){
                            arr_base64 = localStorage.getItem("banner_photo").split(',')
                            mime = arr_base64[0].match(/:(.*?);/)[1]
                            new_ext = mime.split("/")[1]

                            old_ext = filenames["banner"].split('.').pop()

                            file_name = filenames["banner"].split('.')[0]+'.'+new_ext



                            var banner_file = dataURLtoFile(localStorage.getItem("banner_photo"), file_name); 
                            console.log(file_name)
                            // arr[0].match(/:(.*?);/)[1]

                            // ext = localStorage.getItem("banner_photo").split(":")[1].split("/")[1].split(";")[0]
                            // file_name_split = filenames["banner"].split(".")
                            // file_name_split.
                            

                            form_data.append("image", banner_file);
                        }
                        if(localStorage.getItem("upm-img1") != "None" && localStorage.getItem("upm-img1") !== '' && localStorage.getItem("upm-img1").indexOf("https://") === -1){
                            var upm1 = dataURLtoFile(localStorage.getItem("upm-img1"), filenames["upm-img1"])
                            form_data.append("image1", upm1);
                        }
                        if(localStorage.getItem("upm-img2") != "None" && localStorage.getItem("upm-img2") !== '' && localStorage.getItem("upm-img2").indexOf("https://") === -1){
                            var upm2 = dataURLtoFile(localStorage.getItem("upm-img2"), filenames["upm-img2"])
                            form_data.append("image2", upm2);
                        }
                        if(localStorage.getItem("upm-img3") != "None" && localStorage.getItem("upm-img3") !== '' && localStorage.getItem("upm-img3").indexOf("https://") === -1){
                            var upm3 = dataURLtoFile(localStorage.getItem("upm-img3"), filenames["upm-img3"])
                            form_data.append("image3", upm3);
                        }
                    }
                    
                    form_data.append("name", $("#project_title").text().trim())
                    form_data.append("excerpt", $("#excerpt").text().trim())
                    form_data.append("body", $("#full_description").text().trim())


                    for( let k of img_type){
                        
                        form_data.append(`x_${k}`, $(`#x_${k}`).val() );
                        form_data.append(`y_${k}`, $(`#y_${k}`).val() );
                        form_data.append(`w_${k}`, $(`#w_${k}`).val() );
                        form_data.append(`h_${k}`, $(`#h_${k}`).val() );
                    }

                    form_data.append("post_type", 'project_upload');     // type of post mentod in view class
                    $.ajax({
                        beforeSend: function(xhr, settings) {
                            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                                xhr.setRequestHeader("X-CSRFToken", csrftoken);
                            }
                        },
                        url: url,
                        method: 'POST',
                        data: form_data,
                        processData: false,
                        contentType: false,
                        success: function (result) {
                            localStorage.clear();
                            window.location.href='/'
                            
                        }

                    })        


                }
            }
        })

        
        return false;
    });
    
    // clicking submit button on popup of upload page.
    $("#comment_form").click(function() {
        var form_data = new FormData();
        var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
        comments = []
        status = 0
        project_id = null
        $(".error_area").empty()
        // check validations
        project_member_check = check_select_field_validation($("select[name='project_member']").val(), '* Select field is empty.')        
        project_contribution_check = check_field_validation($("textarea[name='comment']").val(), '* Text field is empty.')
        if(!project_member_check || !project_contribution_check) {
            return false;
        }
        if(jQuery("input[name='is_edit']").length > 0) {
            project_id = parseInt(jQuery("input[name='is_edit']").val())
            form_data.append("project_id", project_id)
        }
        project_member = $("select[name='project_member']").val() 
        comment = $("textarea[name='comment']").val()
        if(jQuery("input[name='is_edit']").length == 1) {
            url_redirect = '/upload/'+jQuery("input[name='is_edit']").val() + '/'
        }
        else {
            url_redirect = '/upload/';
        }
        form_data.append("post_type",'author_project_comment')
        form_data.append("project_member", project_member)
        form_data.append("comment", comment)
        jQuery.ajax({
            beforeSend: function(xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            },
            url:url_redirect,
            method: 'POST',
            data: form_data,
            processData: false,
            contentType: false,
            async:false,
            success: function(result) {
                if(jQuery("input[name='is_edit']").length > 0) {
                    if(result.owner_login_status == 1) {
                        alert(result.message)
                    }
                    else {
                        jQuery("#myModal").modal("hide");
                    }
                }
                else {
                    jQuery("#myModal").modal("hide");
                }

                
            }
        })
        return false;
    })



    // put comment in textarea field for already existing authors.
    jQuery("select[name='project_member']").change(function() {
        if(jQuery(this).find(':selected').attr('comment')) {
            comment = jQuery(this).find(':selected').attr('comment')
            jQuery("textarea[name = 'comment']").val(comment)
        }
        else {
            jQuery("textarea[name = 'comment']").val('')
        }

        return false;
    });

    /// for setting correct image files in its corresponding variables
    $("#upload-project-photo").on("change", function () {
        console.log('this.files[0]')
        console.log(this.files[0])
        files["banner"] = this.files[0];
        file_names["banner"] = this.files[0].name;
        localStorage.setItem("banner_file", this.files[0]);

    });

    $('#additional-images .image-input').each(function () {
        $(this).on('change', function () {
            var target_id = $(this).attr('name');
            files[target_id] = this.files[0];
            file_names[target_id] = this.files[0].name;
        })
    });


    // clicking on add/update authors contribution link
    $("#add_author").click(function() {
        already_exist_authors = []     // already existing authors array
        jQuery("select[name='project_member']").html("<option value=''>Select Author</option")

        // select existing authors, it will happen only during edit project page.
        if(jQuery("#authors_area .single_author")) {
            jQuery("#authors_area .single_author").each(function(index, elem) {
                id = jQuery(this).find('span.author-remove').attr('author_id')
                first_name = jQuery(this).find('span.author-remove').attr('first_name')
                last_name = jQuery(this).find('span.author-remove').attr('last_name')
                comment = jQuery(this).find('span.author-remove').attr('comment')
                jQuery("select[name='project_member']").append("<option comment='"+comment+"' value='"+id+"'>"+first_name+" "+last_name+"</option")
                already_exist_authors.push(id)
            });
        }
        
        if(jQuery("input[name='is_edit']").length == 0 && jQuery("#authors_area .single_author").length == 0) {
            jQuery("select[name='project_member']").append("<option value='"+jQuery("input[name='login_owner_id']").val()+"'>"+jQuery("input[name='login_owner_first_name']").val()+" "+jQuery("input[name='login_owner_last_name']").val()+"</option")
        }


        // putting selected authors into dropdown.
        // authors already existing for the current project will not be appended into dropdown of authors contribution dropdown field.
        jQuery(".select2-selection--multiple ul li.select2-selection__choice span").find('input').each(function() {
            is_author_exist = 0;
            data = jQuery(this).attr('data')
            parsed_data = JSON.parse(data)
            if(already_exist_authors.length > 0) {
                for(author of already_exist_authors) {
                    if(author == parsed_data.id) {
                        is_author_exist = 1
                    }
                }
                if(is_author_exist == 0) {
                    jQuery("select[name='project_member']").append("<option value='"+parsed_data.id+"'>"+parsed_data.first_name+" "+parsed_data.last_name+"</option")                    
                }
            }
            else {
                jQuery("select[name='project_member']").append("<option value='"+parsed_data.id+"'>"+parsed_data.first_name+" "+parsed_data.last_name+"</option")
            }
            

        })

        $("select[name='project_member'] option:eq(0)").prop('selected',true)
        $("textarea[name='comment']").val('')
    });
    // like button functionality.
    jQuery(".heart_op").click(function() {
		present_count = jQuery(this).attr("counter");
		project_id = jQuery(this).attr("project_id");
        base_url = window.location.origin
		data = {'project_id':project_id};
		jQuery.ajax({
			url:base_url+'/likes/',
			data: data,
			dataType: 'json',
			success: function(data) {
				if(data.user_like == 0 || data.user_like == 1) {
					if(data.user_like == 0) {
						jQuery("#count_"+present_count).css("color","#E8E6E6");
						jQuery("#count_"+present_count+" span").text(data.total_likes)
					}
					else {
						jQuery("#count_"+present_count).css("color","#FFD500");
						jQuery("#count_"+present_count+" span").text(data.total_likes)
					}
				}
				else {
					alert('Please login to this app first.')
				}
				

			}

		});

	})    

    university_list();
    get_edu_records_length(true, true); // update hidden field for education field records
    get_exp_records_length(true, true)  // update hidden field for work experience field records
});