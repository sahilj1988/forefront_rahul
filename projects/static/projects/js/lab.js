var files = { "upm": [] };
var images = { "upm": [] };
var labFilenames = [];
var labCroppingValues = [];

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function words_limit(text_data, text_length) {
    splitted_text = text_data.split(" ")
    if (splitted_text.length > text_length) {
        return false;
    }
    return true;
}

function dataURLtoFile(dataurl, filename) {

    var arr = dataurl.split(','),
        mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), 
        n = bstr.length, 
        u8arr = new Uint8Array(n);
        
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});
}

function set_img(src, target, callback) {
    var reader = new FileReader();
    reader.addEventListener('load', function (ev) {
        $(target).attr('src', ev.target.result);
        if (callback) {
            callback(ev.target.result);
        }
    });
    reader.readAsDataURL(src);
}
function upload_page_validation(input_field, error_msg) {
    if(!isNaN(input_field)) {
        alert(error_msg);
        return false;
    }
    return true;
}

jQuery("#lab_select").change(function() {
    window.location.href = window.location.origin+'/lab/'+jQuery(this).val()
    return false;
})

splitted_url_str = window.location.pathname.split("/")
// previewed_project_id = splitted_url_str[splitted_url_str.length - 2]

jQuery(document).ready(function() {
    if(window.location.pathname == "/lab/preview/") {
        if(jQuery("#labTopicSliderCarousel .carousel-inner")) {
            jQuery("#labTopicSliderCarousel .carousel-inner").find('.active').not(":eq(0)").removeClass('active')
        }
    }

    if(splitted_url_str[splitted_url_str.length - 4] == 'lab' && splitted_url_str[splitted_url_str.length - 3] == 'edit' && splitted_url_str[splitted_url_str.length - 2] > 0) {
        console.log('here1')
        // document.getElementsByClassName('select2-selection--multiple')

        

        // jQuery('.select2-selection--multiple').eq(0).on('keyup', function (e) {
            
        //     if (e.keyCode == 8) {
        //         console.log('bk')
        //         console.log(jQuery(this).eq(0).find(".select2-search__field").val())
        //         jQuery(this).eq(0).find(".select2-search__field").val('')
                
        //         // jQuery(this).eq(0).find(".select2-search__field").val('')
        //     }
        //     else {
        //         console.log('doesnt exits')
        //     }
        //     return false;
        // });

        var inputs = document.getElementsByClassName('select2-selection--multiple');
        
        // remove extra data when pressing backspace from first dropdown
        inputs[0].onkeydown = function(e) {
            var key = e.keyCode || event.charCode;
            if(key == 8) {
                search_str = jQuery(".select2-search__field").eq(0).val()

                // if searched string is a full user detailed string, then we will compare some parts of the string, if all of them are true then remove that same whole of string. if anyone of them doesn't match, then we will remove string by one character at a time.
                if(search_str.search("input type=") > 0 && search_str.search("class=") > 0 && search_str.search("id=") > 0 && search_str.search("data=") > 0) {
                    jQuery(".select2-search__field").eq(0).val('')
                }
                else {
                    new_str = search_str.substring(0, search_str.length-1)
                    console.log('new_str')
                    console.log(new_str)
                    jQuery(".select2-search__field").eq(0).val('')
                    jQuery(".select2-search__field").eq(0).val(new_str)
                }
                return false;
            }   
        };

        // remove extra data when pressing backspace from second dropdown
        inputs[1].onkeydown = function(e) {
            var key = e.keyCode || event.charCode;
            if(key == 8) {
                search_str = jQuery(".select2-search__field").eq(1).val()

                // if searched string is a full user detailed string, then we will compare some parts of the string, if all of them are true then remove that same whole of string. if anyone of them doesn't match, then we will remove string by one character at a time.
                if(search_str.search("input type=") > 0 && search_str.search("class=") > 0 && search_str.search("id=") > 0 && search_str.search("data=") > 0) {
                    jQuery(".select2-search__field").eq(1).val('')
                }
                else {
                    new_str = search_str.substring(0, search_str.length-1)
                    jQuery(".select2-search__field").eq(1).val('')
                    jQuery(".select2-search__field").eq(1).val(new_str)
                }
                return false;
            }   
        };            



        // check whether admin does exist as a member
        if(jQuery("input[name='is_admin_a_member']").length == 0) {
            // disable outer click
            jQuery('#member_alert').modal({
                backdrop: 'static',
                keyboard: false
            });
            jQuery('#member_alert').modal('show');
            jQuery('body').css({'padding-right': 0});   // remove right side garbage area
        }

        if(localStorage.getItem('is_preview') && localStorage.getItem('is_preview') == 1 && localStorage.getItem('lab_project_id') && localStorage.getItem('lab_project_id') == splitted_url_str[splitted_url_str.length - 2]) {
            jQuery("#lab_title").text(localStorage.getItem("lab_title"))
            jQuery("#university-name").val(localStorage.getItem("selected_university"))
            jQuery("#excerpt").text(localStorage.getItem("excerpt"))
            jQuery("#major-contribution").text(localStorage.getItem("major_contribution"))
            jQuery("#source_of_funding").text(localStorage.getItem("source_of_funding"))

            console.log(localStorage.getItem("authors_arr"));

            jQuery(".author-row").eq(0).children().eq(1).remove()
            // jQuery(".author-row").eq(0).prepend()
            professors_data = '<div class="author-container">';
            jQuery.each(JSON.parse(localStorage.getItem("professors_arr")), function (index, professor) {
                professors_data += '<div class="author-inner professor_detail"><div class="group author-img-container profile"><img src="'+professor.image+'" alt="" class="author-image"></div><div class="author-profile-info"><div class="group-lead-container"><div class="d-flex" style="display: flex; justify-content: space-between;align-items: center;"><a href="/author/'+professor.id+'"><span class="author-name">'+professor.author_name+'</span></a></div><div class="clearfix"></div><div class="position forefront-whitespace">'+professor.position+'</div><div></div></div></div><div class="student-remove"><span class="glyphicon glyphicon-remove" style="color: #FED501;font-size: .8em;"></span></div></div>'
            });
            professors_data += '</div>';
            jQuery(".author-row").eq(0).prepend(professors_data)
            
            // jQuery(".author-row").eq(1).children().eq(1).remove()
            jQuery(".authors-container").remove()

            authors_data = '<div class="authors-container">';
            admin_member_id = parseInt(jQuery("input[name='is_admin_a_member']").val())
            jQuery.each(JSON.parse(localStorage.getItem("authors_arr")), function (index, author) {
                display_value = null
                if(admin_member_id == parseInt(author.id)) {
                    display_value = 'None'
                }
                else {
                    display_value = 'Block'
                }
                a = ''
                authors_data += '<div class="author-container"><div class="author-inner student_detail"><div class="group author-img-container profile"><img src="' + author.image + '" alt="" class="author-image"></div><div class="author-profile-info"><div class="group-lead-container"><a href="/author/' + author.id + '"><div class="author-name">' + author.author_name + '</div></a><div class="position forefront-whitespace">' + author.position + '</div><div></div></div></div><div class="student-remove" style="display:'+display_value+'"><span class="glyphicon glyphicon-remove" style="color: #FED501;font-size: .8em;"></span></div></div></div>'
            });
            authors_data += '</div>';
            jQuery(".author-row").eq(1).prepend(authors_data)

            publications_arr = JSON.parse(localStorage.getItem("publications_arr"))
            if(publications_arr.length > 0) {
                jQuery(".lab-publication-list .lab-publication-row").remove()
                publication_data = ''
                jQuery("#publication_records_length").val(publications_arr.length)
                jQuery.each(publications_arr, function (index, publication) {
                    publication_data += '<div class="row lab-publication-row form-group forefront-lab-col"><div class="col-12"><textarea name="publication_text'+index+'" class="publication_text" id="publication_text'+index+'" rows="3" placeholder="Enter Lab Publications">'+publication.citation+'</textarea></div><a href="javascript:void(0)" class="lab-publication-remove-btn" style="color:yellow; float:left;"><span class="glyphicon glyphicon-remove" style="color: #FED501;font-size: .8em;"></span></a></div>'
                });                
                
                jQuery(".lab-publication-list").append(publication_data)

            }

            lab_research_list = JSON.parse(localStorage.getItem("lab_research_list"))
            if(lab_research_list.length > 0) {
                jQuery(".lab-research-list .lab-research-row").remove()
                lab_data = ''
                
                jQuery.each(lab_research_list, function (index, lab) {
                    if(lab.image == "same") {
                        lab_data += '<div class="row lab-research-row form-group" id="'+lab.id+'"><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><input type="text" class="lab_research_name" name="lab_research_name'+index+'_0" id="lab_research_name" value="'+lab.name+'" data-lab="'+lab.id+'" placeholder="Lab Research Topic Name"></div><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><input type="text" name="lab_research_desc'+index+'_1" class="lab_reseacrh_desc" rafael="abc" id="lab_reseacrh_desc" placeholder="Lab Research Excerpt" value="'+lab.excerpt+'"></div><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><label for="lab_research_image'+index+'_2" class="text-center"> <i class="glyphicon glyphicon-picture"></i> Choose a Photo<input type="file" class="lab_research_image" name="lab_research_image'+index+'_2" id="lab_research_image'+index+'_2" style="display: none;" value=""></label></div><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><div class="preview-image" id="preview-image'+index+'_3" style="position: relative; padding: 10px 10px 0 10px;">Preview Image Here.<img src="'+lab.image_url+'" alt="" class="img-fluid" style="position: absolute; top: 0; left: 0;"></div></div><a href="javascript:void(0)" class="lab-research-remove-btn" style="color:yellow; float:left;"><span class="glyphicon glyphicon-remove" style="color: #FED501;font-size: .8em;"></span></a></div>'
                    }
                    else {
                        lab_image = localStorage.getItem(lab.image)
                        lab_csstext = localStorage.getItem(lab.image+"_small_preview_cssText")
                        
                        lab_data += '<div class="row lab-research-row form-group" id="'+lab.id+'"><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><input type="text" class="lab_research_name" name="lab_research_name'+index+'_0" id="lab_research_name" value="'+lab.name+'" data-lab="'+lab.id+'" placeholder="Lab Research Topic Name"></div><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><input type="text" name="lab_research_desc'+index+'_1" class="lab_reseacrh_desc" rafael="abc" id="lab_reseacrh_desc" placeholder="Lab Research Excerpt" value="'+lab.excerpt+'"></div><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><label for="lab_research_image'+index+'_2" class="text-center"> <i class="glyphicon glyphicon-picture"></i> Choose a Photo<input type="file" class="lab_research_image" name="lab_research_image'+index+'_2" id="lab_research_image'+index+'_2" style="display: none;" value=""></label></div><div class="col-lg-3" style="padding: 0; margin: 0 1em;"><div class="preview-image" id="preview-image'+index+'_3" style="position: relative; padding: 0; width: 223.27px;"><img src="'+lab_image+'" alt="" class="img-fluid" style="'+lab_csstext+'"></div></div><a href="javascript:void(0)" class="lab-research-remove-btn" style="color:yellow; float:left;"><span class="glyphicon glyphicon-remove" style="color: #FED501;font-size: .8em;"></span></a></div>'
                    }

                });
                jQuery(".lab-research-list").append(lab_data)
                jQuery("#research_records_length").val(lab_research_list.length)
            }
        }
    }        
})


jQuery('#member_alert').on("click",'.profile_redirect_btn', function () {
    window.location.href = window.location.origin+'/profile'
    return false;
})

jQuery("#back_btn").click(function() {
    window.location.href = window.location.origin+'/lab/edit/'+localStorage.getItem("lab_project_id")+'/'
    return false;
})

// jQuery(".carousel-caption").on("click", ".lab_edit_link",function() {
jQuery(".lab_edit_link").click(function() {
    base_url = window.location.origin
    edit_link = jQuery(this).attr('href')        
    localStorage.clear()
    jQuery.ajax({
        url:base_url+'/remove-sessions/',
        dataType: 'json',
        success: function(data) {
            console.log(data.message)
            window.location.href = window.location.origin+edit_link

            return false;
        }

    });
    
    return false;        
});

$('#major-contribution').on('keyup', function (e) {
    var n = $(this).text().trim().split(' ').length;
    if (e.keyCode == 8) {

    } else if (n > 500) {
        alert('Words length is not allowed more than 500 for major contribution text field.');
        $(this).text($('#major-contribution').text().trim().split(' ').slice(0, 500).join(' '));
        n = $(this).text().trim().split(' ').length;
    }
    $(this).addClass('changed').attr('data-content', 500 - n);
    console.log(n);
});


$('#source_of_funding').on('keyup', function (e) {
    var n = $(this).text().trim().split(' ').length;
    if (e.keyCode == 8) {

    } else if (n > 500) {
        alert('Words length is not allowed more than 500 for source of funding text field.');
        $(this).text($('#source_of_funding').text().trim().split(' ').slice(0, 500).join(' '));
        n = $(this).text().trim().split(' ').length;
    }
    $(this).addClass('changed').attr('data-content', 500 - n);
    console.log(n);
});


$('.lab-research-list .lab_research_image').each(function () {

    if ($(this).attr("value") !== undefined) {
        files[$(this).attr("name")] = "same";
    }
    // after coming from preview page update it
    if(localStorage.getItem('is_preview') == 1){
        var filenames = JSON.parse(localStorage.getItem("lab_file_names"));
        for (const elm of filenames) {
            let name = elm['name'] ;
            files[name] = elm['filename'];
        }
    }

    $(this).on("change", function () {
        var target_id = $(this).parent().parent().next().children().attr("id");
        files[$(this).attr("name")] = this.files[0];
        labFilenames.push({
            'name': $(this).attr('name'),
            'filename': this.files[0].name,
        });
        // set_img(this.files[0], "#" + target_id + " img", function (result) {
        //     images[$(this).attr('name')] = result;
        // })
    });
});


var researchNew = $('#lab-reseacrh-new').html();
$('#lab-research-add-btn').click(function () {
    var add = true;
    $('.lab-research-list .lab_research_name').each(function () {
        if ($(this).val() === '') {
            this.focus();
            add = false;
            return false;
        }
    });
    $('.lab-research-list .preview-image').each(function () {
        var src = $(this).children().attr("src");
        if (src === '') {
            $(this).parent().parent().children().eq(2).children().addClass('focus');
            add = false;
            return false;
        } else {
            $(this).parent().parent().children().eq(2).children().removeClass('focus');
        }
    });
    if (add === true) {
        $('.lab-research-list').append(researchNew);
        row_index = parseInt($('#research_records_length').val());

        $('.lab-research-list .row.lab-research-row.form-group').last().children().first().children().attr("name", "lab_research_name" + row_index + "_0");
        $('.lab-research-list .row.lab-research-row.form-group').last().children().eq(1).children().attr("name", "lab_research_desc" + row_index + "_1");
        $('.lab-research-list .row.lab-research-row.form-group').last().children().eq(2).children().attr('for', 'lab_research_image' + row_index + '_2')
        $('.lab-research-list .row.lab-research-row.form-group').last().children().eq(2).children().children().eq(1).attr("name", "lab_research_image" + row_index + "_2");
        $('.lab-research-list .row.lab-research-row.form-group').last().children().eq(2).children().children().eq(1).attr("id", "lab_research_image" + row_index + "_2");
        $('.lab-research-list .row.lab-research-row.form-group').last().children().eq(3).children().attr("id", "preview-image" + row_index + "_3");

        $('#research_records_length').val(parseInt($('#research_records_length').val()) + 1);

        $('.lab-research-list .lab_research_image').each(function () {
            $(this).on("change", function () {
                var target_id = $(this).parent().parent().next().children().attr("id");
                files[$(this).attr("name")] = this.files[0];
                labFilenames.push({
                    'name': $(this).attr('name'),
                    'filename': this.files[0].name,
                });
                // set_img(this.files[0], "#" + target_id + " img", function (result) {
                //     images[$(this).attr('name')] = result;
                // })
            });
        });

        $('.lab-research-list .lab_research_image').each(function () {
            $(this).on("change", function () {
                if (this.files && this.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function (e) {
                        $("#imgModal").attr("src", e.target.result);
                        $("#labImgModal").modal("show");
                    }
                    reader.readAsDataURL(this.files[0]);
                    // console.log($(this).attr('name'));
                    lab_img_for = $(this).attr('name');
                }
            });
        });

    }

});

$('.lab-research-list').on('click', ".lab-research-remove-btn", function () {
    $(this).parent('.lab-research-row').remove();
});


// add publication firld
var publicationNew = $('#lab-publication-new').html();
$('#lab-publication-add-btn').click(function () {
    var add = true;
    $('.lab-publication-list .publication_text').each(function () {
        if ($(this).val() === '') {
            this.focus();
            add = false;
            return false;
        }
    });
    if (add === true) {
        $('.lab-publication-list').append(publicationNew);
        row_index = parseInt($('#publication_records_length').val());

        $('.lab-publication-list .row.lab-publication-row.form-group').last().children().children().attr('name', 'publication_text' + row_index);

        $('#publication_records_length').val(parseInt($('#publication_records_length').val()) + 1);
    }

})

// on clicking - button for lab publications
$('.lab-publication-list').on('click', ".lab-publication-remove-btn", function () {
    $(this).parent('.lab-publication-row').remove();
});

function get_professors_list() {
    authors_arr = []
    temp_professors_arr = []
    professors_arr = []
    is_professor_exist = 0
    if (jQuery(".professor_detail").length > 0) {
        jQuery(".professor_detail").each(function (elem) {
            href_split = jQuery(this).find("a").attr("href").split("/")

            id = href_split[2]
            author_name = jQuery(this).find(".author-name").text().trim()
            position = jQuery(this).find(".forefront-whitespace").text().trim()
            image = jQuery(this).find("img").attr('src')

            temp_professors_arr.push({ id: id, author_name: author_name, position: position, image: image })
        })
        if (jQuery(".professors .select2-selection--multiple ul li.select2-selection__choice span").length > 0) {
            professors_arr = temp_professors_arr
            jQuery(".professors .select2-selection--multiple ul li.select2-selection__choice span").find('input').each(function () {
                for (prof of temp_professors_arr) {
                    if (parseInt(prof.id) == parseInt(jQuery(this).attr('id'))) {
                        is_professor_exist = 1
                        break;
                    }
                }
                if (is_professor_exist == 0) {
                    id = jQuery(this).attr('id')
                    parsed_data = JSON.parse(jQuery(this).attr('data'))
                    first_name = parsed_data.first_name
                    last_name = parsed_data.last_name
                    position = parsed_data.position
                    image = parsed_data.image

                    professors_arr.push({ id: id, author_name: first_name + ' ' + last_name, position: position, image: image })
                }

            });

        }
        else {
            professors_arr = temp_professors_arr
        }

    }
    else if (jQuery(".professors .select2-selection--multiple ul li.select2-selection__choice span").length > 0) {
        jQuery(".professors .select2-selection--multiple ul li.select2-selection__choice span").find('input').each(function () {
            id = jQuery(this).attr('id')
            parsed_data = JSON.parse(jQuery(this).attr('data'))
            first_name = parsed_data.first_name
            last_name = parsed_data.last_name
            position = parsed_data.position
            image = parsed_data.image

            professors_arr.push({ id: id, author_name: first_name + ' ' + last_name, position: position, image: image })
        });
    }
    return JSON.stringify(professors_arr)
}


function get_students_list() {
    authors_arr = []
    temp_students_arr = []
    is_student_exist = 0
    if (jQuery(".student_detail").length > 0) {
        jQuery(".student_detail").each(function (elem) {
            href_split = jQuery(this).find("a").attr("href").split("/")
            id = href_split[2]
            author_name = jQuery(this).find(".author-name").text().trim()
            position = jQuery(this).find(".forefront-whitespace").text().trim()
            image = jQuery(this).find("img").attr('src')

            temp_students_arr.push({ id: id, author_name: author_name, position: position, image: image })
        })
        if (jQuery(".members .select2-selection--multiple ul li.select2-selection__choice span").length > 0) {
            students_arr = temp_students_arr
            jQuery(".members .select2-selection--multiple ul li.select2-selection__choice span").find('input').each(function () {
                for (student of temp_students_arr) {
                    if (parseInt(student.id) == parseInt(jQuery(this).attr('id'))) {
                        is_student_exist = 1
                        break
                    }
                }
                if (is_student_exist == 0) {
                    id = jQuery(this).attr('id')
                    parsed_data = JSON.parse(jQuery(this).attr('data'))
                    first_name = parsed_data.first_name
                    last_name = parsed_data.last_name
                    position = parsed_data.position
                    image = parsed_data.image

                    students_arr.push({ id: id, author_name: first_name + ' ' + last_name, position: position, image: image })
                }

            });

        }
        else {
            students_arr = temp_students_arr
        }

    }
    else if (jQuery(".members .select2-selection--multiple ul li.select2-selection__choice span").length > 0) {
        jQuery(".members .select2-selection--multiple ul li.select2-selection__choice span").find('input').each(function () {

            id = jQuery(this).attr('id')
            parsed_data = JSON.parse(jQuery(this).attr('data'))
            first_name = parsed_data.first_name
            last_name = parsed_data.last_name
            position = parsed_data.position
            image = parsed_data.image
            students_arr.push({ id: id, author_name: first_name + ' ' + last_name, position: position, image: image })
        });
    }

    return JSON.stringify(students_arr)
}


var selectedUni = ''
$('select#university-name').change(function () {
    selectedUni = $(this).children('option:selected').attr('name');
})

// clicking on preview button on lab upload or edit page.
var win = "";
$("#lab-preview-btn").click(function () {
    authors_arr = []
    students_arr = []
    var form_data = new FormData();

    // input fields
    
    lab_title_check = upload_page_validation($("#lab_title").text().trim(), 'Lab title field is empty.')
    if (!lab_title_check) {
        return false;
    }

    lab_university_check = $('#university-name').val() !== '' ? true : alert('University is Empty!')
    if (!lab_university_check) {
        return false;
    }

    excerpt_check = upload_page_validation($("#excerpt").text().trim(), 'Excerpt field is empty.')
    if (!excerpt_check) {
        return false;
    }

    var research_status = 0;
    $(".lab-research-list .lab-research-row").each(function () {
        if ($(this).find(".lab_research_name").val() == '' || $(this).find(".lab_research_desc") == '') {
            research_status = 1;
            alert('Please add Lab Research Topic fields');
            return false;
        }
        if ($(this).find('.preview-image img').attr("src") == '') {
            research_status = 1
            alert("Please add Lab Research images!")
            return false;
        }
    });
    if (research_status == 1) {
        return false;
    }

    var publication_status = 0;
    $(".lab-publication-list .lab-publication-row").each(function () {
        if ($(this).find(".publication_text").val() == '') {
            publication_status = 1;
            alert('Please add Lab Publication fields');
            return false;
        }
    });
    if (publication_status == 1) {
        return false;
    }


    major_contribution_check = upload_page_validation($("#major-contribution").text().trim(), 'Major Contribution field is empty.')
    if (!major_contribution_check) {
        return false;
    }

    funding_check = upload_page_validation($("#source_of_funding").text().trim(), 'Source of Funding field is empty.')
    if (!funding_check) {
        return false;
    }

    text_limit = words_limit($("#excerpt").text().trim(), 100)
    if (!text_limit) {
        alert('Words length is not allowed more than 100 for excerpt text field.');
        return false;
    }

    text_limit = words_limit($("#major-contribution").text().trim(), 500)
    if (!text_limit) {
        alert('Words length is not allowed more than 500 for major contribution text field.');
        return false;
    }

    text_limit = words_limit($("#source_of_funding").text().trim(), 500)
    if (!text_limit) {
        alert('Words length is not allowed more than 500 for source of funding text field.');
        return false;
    }

    // validation on students dropdown.
    if (jQuery(".student_detail").length == 0 && jQuery(".members .select2-selection--multiple ul li.select2-selection__choice span").length == 0) {
        alert('Please select atleast one student.')
        return false;

    }

    // professors
    if (jQuery(".professor_detail").length == 0 && jQuery(".professors .select2-selection--multiple ul li.select2-selection__choice span").find('input').length == 0) {
        alert("Please select atleast one professor");
        return false;
    }
    // assign selected students
    students_arr = get_students_list()
    professors_arr = get_professors_list()
    var csrftoken = jQuery("input[name=csrfmiddlewaretoken]").val();
    localStorage.setItem("lab_title", $("#lab_title").text().trim());
    localStorage.setItem("university_name", $("select#university-name").children('option:selected').attr('name'));

    
    localStorage.setItem("selected_university", $('select#university-name').val());
    localStorage.setItem("excerpt", $("#excerpt").text().trim());


    localStorage.setItem("major_contribution", $("#major-contribution").text().trim());
    localStorage.setItem("source_of_funding", $("#source_of_funding").text().trim());

    form_data.append("authors_arr", students_arr)
    localStorage.setItem("authors_arr", students_arr);
    localStorage.setItem("professors_arr", professors_arr);

    var update = parseInt($("#update").val());
    if (update) {
        localStorage.setItem("update", true);
        // for lab research topics
        var total = parseInt($('#research_records_length').val());
        var research_arr = [];
        let c = 0;
        for (let i = 0; i < total; i++) {
            if ($('input[name=lab_research_name' + i + '_0]').val() === undefined || $('input[name=lab_research_desc' + i + '_1]').val() === undefined) {
                ;
            } else {
                var image_url = '';
                id = parseInt($('input[name=lab_research_name' + i + '_0]').attr("data-lab"));
                name = $('input[name=lab_research_name' + i + '_0]').val();
                excerpt = $('input[name=lab_research_desc' + i + '_1]').val();
                if (update && !isNaN(id)) {
                    if (files["lab_research_image" + i + "_2"] === 'same') {

                        image = 'same';
                        image_url = $('#preview-image' + i + '_3').children().attr('src');

                    } else {

                        localStorage.setItem('update_image_' + id, $("#preview-image" + i + '_3').children().attr("src"));
                        image = 'update_image_' + id;

                        localStorage.setItem(image + "-cssText", $("#lab_research_image" + i + "_2-cssText").val());
                        // console.log(localStorage.getItem(image+"-cssText"));
                        // alert('_lab.html 1376');

                    }
                }
                else {
                    localStorage.setItem('image_' + c, $("#preview-image" + i + '_3').children().attr("src"));
                    localStorage.setItem('image_' + c +'_small_preview_cssText', $("#preview-image" + i + '_3').children().attr("style"));
                    image = 'image_' + c;
                    localStorage.setItem(image + "-cssText", $("#lab_research_image" + i + "_2-cssText").val());
                    c += 1;
                }
                if (image_url !== '') {
                    research_arr.push({ id, name, excerpt, image, image_url });
                } else {
                    research_arr.push({ id, name, excerpt, image });
                }
            }
        }
        form_data.append('lab_research_list', JSON.stringify(research_arr));
        localStorage.setItem('lab_research_list', JSON.stringify(research_arr))
        // for lab publication
        var total = parseInt($('#publication_records_length').val());
        var publication_arr = [];
        for (let i = 0; i < total; i++) {
            if (citation = $('textarea[name=publication_text' + i + ']').val() === undefined) {
                ;
            } else {
                citation = $('textarea[name=publication_text' + i + ']').val();
                publication_arr.push({ citation });
            }
            $('textarea[name=publication_text' + i + ']').val()
        }
        form_data.append('lab_publication_list', JSON.stringify(publication_arr));
        
        localStorage.setItem('publications_arr', JSON.stringify(publication_arr))
    }
    else {
        // for lab research topics
        var total = parseInt($('#research_records_length').val());
        var research_arr = [];
        let c = 0;
        for (let i = 0; i < total; i++) {
            if ($('input[name=lab_research_name' + i + '_0]').val() === undefined || $('input[name=lab_research_desc' + i + '_1]').val() === undefined) {
                ;
            } else {
                name = $('input[name=lab_research_name' + i + '_0]').val();
                excerpt = $('input[name=lab_research_desc' + i + '_1]').val();
                localStorage.setItem('image_' + c, $("#preview-image" + i + '_3').children().attr("src"));
                c += 1;
            }
        }
        localStorage.setItem('total_images', c);
        form_data.append('lab_research_list', JSON.stringify(research_arr));

        // for lab publication
        var total = parseInt($('#publication_records_length').val());
        var publication_arr = [];
        for (let i = 0; i < total; i++) {
            if (citation = $('textarea[name=publication_text' + i + ']').val() === undefined) {
                ;
            } else {
                citation = $('textarea[name=publication_text' + i + ']').val();
                publication_arr.push({ citation });
            }
        }
        form_data.append('lab_publication_list', JSON.stringify(publication_arr));
    }
    // set cropping values in localStorage
    var total_values = $(".cropping-hidden-input-box").children().length;
    for (let x = 0; x < total_values; x++) {
        var obj = {
            'name': $(".cropping-hidden-input-box").children().eq(x).children().first().attr('name').slice(2),
            'x': $(".cropping-hidden-input-box").children().eq(x).children().eq(0).val(),
            'y': $(".cropping-hidden-input-box").children().eq(x).children().eq(1).val(),
            'w': $(".cropping-hidden-input-box").children().eq(x).children().eq(2).val(),
            'h': $(".cropping-hidden-input-box").children().eq(x).children().eq(3).val(),
            'cssText': $(".cropping-hidden-input-box").children().eq(x).children().eq(4).val(),
            // can also set cssText 
        }

        labCroppingValues.push(obj);
    }

    localStorage.setItem('lab_cropping_values', JSON.stringify(labCroppingValues));
    localStorage.setItem('lab_file_names', JSON.stringify(labFilenames));
    form_data.append('students_arr', students_arr);
    form_data.append('professors_arr', professors_arr);
    form_data.append("post_type", "lab_upload_preview");

    var url = window.location.pathname;

    jQuery.ajax({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        url: url,
        method: 'POST',
        data: form_data,
        processData: false,
        contentType: false,
        async: false,
        success: function (result) {
            if (win == "" || win.closed == true) {
                localStorage.setItem('is_preview', 1);
                localStorage.setItem('lab_project_id', splitted_url_str[splitted_url_str.length - 2]);
                win = window.open('/lab/preview', '_self');
            } else {
                win.location.reload();
                win.focus();
            }
        }
    })


    return false;
});




/// publish or update btn


$("#lab-publish-btn").click(function () {
    var form_data = new FormData();
    authors_arr = [];
    students_arr = [];
    professors_arr = [];
    var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

    // cut this 
    if (localStorage.getItem('is_preview') == 1) {
        var values = JSON.parse(localStorage.getItem('lab_cropping_values'));
        for (const elm of values) {
            name = elm['name'];
            // add html
            $(".cropping-hidden-input-box").append($(".new-hidden-input-box").html());

            $(".cropping-hidden-input-box").children().last().children().first().attr({
                name: "x_" + name,
                id: "x_" + name,
                value: elm['x'],
            });
            $(".cropping-hidden-input-box").children().last().children().eq(1).attr({
                name: "y_" + name,
                id: "y_" + name,
                value: elm['y']
            });
            $(".cropping-hidden-input-box").children().last().children().eq(2).attr({
                name: "w_" + name,
                id: "w_" + name,
                value: elm['w']
            });
            $(".cropping-hidden-input-box").children().last().children().eq(3).attr({
                name: "h_" + name,
                id: "h_" + name,
                value: elm['h']
            });
            $(".cropping-hidden-input-box").children().last().children().eq(4).attr({
                name: name + "-cssText",
                id: name + "-cssText",
                value: elm['cssText']
            });

        }
    }

    // till here into

    // alert('1558');
    // input fields

    lab_title_check = upload_page_validation($("#lab_title").text().trim(), 'Lab title field is empty.')
    if (!lab_title_check) {
        return false;
    }

    lab_university_check = $('#university-name').val() !== '' ? true : alert('University fields is Empty!')
    if (!lab_university_check) {
        return false;
    }

    excerpt_check = upload_page_validation($("#excerpt").text().trim(), 'Excerpt field is empty.')
    if (!excerpt_check) {
        return false;
    }

    text_limit = words_limit($("#excerpt").text().trim(), 100)
    if (!text_limit) {
        alert('Words length is not allowed more than 100 for excerpt text field.');
        return false;
    }

    text_limit = words_limit($("#major-contribution").text().trim(), 500)
    if (!text_limit) {
        alert('Words length is not allowed more than 500 for major contribution text field.');
        return false;
    }

    text_limit = words_limit($("#source_of_funding").text().trim(), 500)
    if (!text_limit) {
        alert('Words length is not allowed more than 500 for source of funding text field.');
        return false;
    }


    var research_status = 0;
    $(".lab-research-list .lab-research-row").each(function () {
        if ($(this).find(".lab_research_name").val() == '' || $(this).find(".lab_research_desc") == '') {
            research_status = 1;
            alert('Please add Lab Research Topic fields');
            return false;
        }
        if ($(this).find('.preview-image img').attr("src") == '') {
            research_status = 1
            alert("Please add Lab Research images!")
            return false;
        }
    });
    if (research_status == 1) {
        return false;
    }

    var publication_status = 0;
    $(".lab-publication-list .lab-publication-row").each(function () {
        if ($(this).find(".publication_text").val() == '') {
            publication_status = 1;
            alert('Please add Lab Publication fields');
            return false;
        }
    });
    if (publication_status == 1) {
        return false;
    }

    major_contribution_check = upload_page_validation($("#major-contribution").text().trim(), 'Major Contribution field is empty.')
    if (!major_contribution_check) {
        return false;
    }

    funding_check = upload_page_validation($("#source_of_funding").text().trim(), 'Source of Funding field is empty.')
    if (!funding_check) {
        return false;
    }

    // atleast one author should be selected from dropdown.
    // validation on students dropdown.
    // validation on students dropdown.
    if (jQuery(".student_detail").length == 0 && jQuery(".members .select2-selection--multiple ul li.select2-selection__choice span").length == 0) {
        alert('Please select atleast one student.')
        return false;

    }

    // professors
    if (jQuery(".professor_detail").length == 0 && jQuery(".professors .select2-selection--multiple ul li.select2-selection__choice span").find('input').length == 0) {
        alert("Please select atleast one professor");
        return false;
    }


    // assign selected students
    students_arr = get_students_list()
    professors_arr = get_professors_list()

    // ajax to check comments for authors
    // form_data.append("post_type", 'author_save_temp')
    form_data.append("authors_arr", JSON.stringify(authors_arr))


    url = window.location.pathname;

    jQuery.ajax({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        },
        url: url,
        method: 'POST',
        data: form_data,
        processData: false,
        contentType: false,
        async: false,
        success: function (result) {
            form_data.append("authors_arr", students_arr)
            form_data.append("name", $("#lab_title").text().trim())
            form_data.append("university", parseInt($('#university-name').val()))
            form_data.append("excerpt", $("#excerpt").text().trim())
            form_data.append('major_contri', $("#major-contribution").text().trim())
            form_data.append("source_of_funding", $("#source_of_funding").text().trim());
            form_data.append("professors_arr", professors_arr)
            // for lab research topics
            var total = parseInt($('#research_records_length').val());
            var update = parseInt($("#update").val());


            var research_arr = [];
            let c = 0;
            for (let i = 0; i < total; i++) {
                if ($('[name=lab_research_name' + i + '_0]').val() === undefined || $('[name=lab_research_desc' + i + '_1]').val() === undefined) {
                    // alert("continue")
                    ;
                } else {
                    id = parseInt($('[name=lab_research_name' + i + '_0]').attr("data-lab"));
                    name = $('[name=lab_research_name' + i + '_0]').val();
                    excerpt = $('[name=lab_research_desc' + i + '_1]').val();

                    
                    var file_name = '';
                    if(localStorage.getItem("is_preview") == 1){
                        var lab_filenames = JSON.parse(localStorage.getItem("lab_file_names"));
                        for (const elm of lab_filenames) {
                            if (elm['name'] == 'lab_research_image' + i + '_2') {
                                file_name = elm['filename'];
                            }
                        }
                    }
                    
                    if (update && !isNaN(id)) {
                        if (files["lab_research_image" + i + "_2"] === 'same') {
                            image = 'same';
                        } else {
                            image = 'update_image_' + id;
                            var image_file;
                            if(localStorage.getItem('is_preview') == 1 && localStorage.getItem(image) != null){
                                image_file = dataURLtoFile(localStorage.getItem(image), file_name);
                            }else{
                                image_file = files["lab_research_image" + i + "_2"];
                            }
                            form_data.append(image, image_file);
                            // alert('1748');
                            console.log(form_data.get(image));

                            //cropping values
                            form_data.append('x_' + image, $('#x_lab_research_image' + i + '_2').val());
                            form_data.append('y_' + image, $('#y_lab_research_image' + i + '_2').val());
                            form_data.append('w_' + image, $('#w_lab_research_image' + i + '_2').val());
                            form_data.append('h_' + image, $('#h_lab_research_image' + i + '_2').val());
                        }
                    } else {
                        image = 'image_' + c;
                        var image_file;
                        if(localStorage.getItem("is_preview") == 1 && localStorage.getItem(image) != null){  
                            image_file = dataURLtoFile(localStorage.getItem(image), file_name);                                
                        }else{
                            image_file = files["lab_research_image" + i + "_2"];
                        }
                        form_data.append(image, image_file);
                        c += 1;

                        //cropping values
                        form_data.append('x_' + image, $('#x_lab_research_image' + i + '_2').val());
                        form_data.append('y_' + image, $('#y_lab_research_image' + i + '_2').val());
                        form_data.append('w_' + image, $('#w_lab_research_image' + i + '_2').val());
                        form_data.append('h_' + image, $('#h_lab_research_image' + i + '_2').val());
                    }
                    research_arr.push({ id, name, excerpt, image });
                }
            }
            form_data.append('lab_research_list', JSON.stringify(research_arr));


            // for lab publication
            var total = parseInt($('#publication_records_length').val());
            var publication_arr = [];
            for (let i = 0; i < total; i++) {
                if (citation = $('[name=publication_text' + i + ']').val() === undefined) {
                    ;
                } else {
                    citation = $('[name=publication_text' + i + ']').val();
                    publication_arr.push({ citation });
                }
            }

            if (update) {
                form_data.append('students_arr', students_arr);
            }
            form_data.append('lab_publication_list', JSON.stringify(publication_arr));
            form_data.append("post_type", 'lab_upload');

            $.ajax({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                        xhr.setRequestHeader("X-CSRFToken", csrftoken);
                    }
                },
                url: url,
                method: 'POST',
                data: form_data,
                processData: false,
                contentType: false,
                success: function (result) {
                    window.location.href = '/labs'

                }

            })

        }
    })
    return false;
});
