let cropper, picture_for, lab_img_for;

function convertExponentialToDecimal(exponentialNumber) {
    // sanity check - is it exponential number
    const str = exponentialNumber.toString();
    if (str.indexOf('e') !== -1) {
        const exponent = parseInt(str.split('-')[1], 10);
        const result = exponentialNumber.toFixed(exponent);
        return result;
    } else {
        return exponentialNumber;
    }
}

function myPreview(cropper, picture_for) {
    console.log('here1')
    var viewBox = document.getElementsByClassName('img-preview-' + picture_for)[0];



    var imageData = cropper.getImageData(),
        canvasData = cropper.getCanvasData(),
        cropBoxData = cropper.getCropBoxData();
    var cropBoxWidth = cropBoxData.width,
        cropBoxHeight = cropBoxData.height;
    var width = imageData.width,
        height = imageData.height;

    
    // document.getElementById("img-final-banner").appendChild(img);
        
    // console.log(cropBoxData.left,canvasData.left,imageData.left);
    var left = convertExponentialToDecimal(cropBoxData.left - canvasData.left - imageData.left);
    var top = convertExponentialToDecimal(cropBoxData.top - canvasData.top - imageData.top);
    // console.log(left, top);

    var imgPreivewElement = $('.img-preview-' + picture_for).children()[0];
    // if(! $('.img-preview-'+ picture_for).children().length){
    //     imgPreivewElement = document.createElement('img');
    //     imgPreivewElement.id = picture_for+"_image";
    //     viewBox.appendChild(imgPreivewElement);
    // }else{
    //     imgPreivewElement = ;
    // }
    imgPreivewElement.style.display = "inline-block";
    imgPreivewElement.src = $("#imgModal").attr("src");
    imgPreivewElement.style.cssText = 'display:block;' + 'width:100%;' + 'height:auto;' + 'min-width:0!important;' + 'min-height:0!important;' + 'max-width:none!important;' + 'max-height:none!important;' + 'image-orientation:0deg!important;"';

    // $('.img-preview-'+ picture_for).css({
    //     'width': width+'px',
    //     'height': height + 'px',
    //     'transform': `translateX(-${left}px), translateY(-${top}px)`
    // })

    var data = $('.img-preview-' + picture_for);
    // console.log(data);
    var originalWidth = data.width();
    var originalHeight = data.height();
    var newWidth = originalWidth;
    var newHeight = originalHeight;
    var ratio = 1;

    if (cropBoxWidth) {
        ratio = originalWidth / cropBoxWidth;
        newHeight = cropBoxHeight * ratio;
    }

    if (cropBoxHeight && newHeight > originalHeight) {
        ratio = originalHeight / cropBoxHeight;
        newWidth = cropBoxWidth * ratio;
        newHeight = originalHeight;
    }
    
    if (picture_for === 'banner') {
        $(".project-banner-img").css({
            'height': newHeight + 'px'
        })
    }

    $('.img-preview-' + picture_for).children().css({
        'width': width * ratio + 'px',
        'height': height * ratio + 'px',
        'transform': `translateX(${-left * ratio}px) translateY(-${top * ratio}px)`
    });
    // console.log('it is');
    // console.log(`${left*ratio}, ${top*ratio}`);

}

function labPreview(cropper, lab_img_for) {
    var boxIndex = lab_img_for.slice(-3, -2);
    var viewBox = $('#preview-image' + boxIndex + '_3');
    // remove padding and innerText while retaining children html tags
    var child = viewBox.children()[0];
    viewBox.text('');
    viewBox.css({
        'padding': 0
    });
    viewBox.html(child);


    var imageData = cropper.getImageData(),
        canvasData = cropper.canvasData,
        cropBoxData = cropper.cropBoxData;
    var cropBoxWidth = cropBoxData.width,
        cropBoxHeight = cropBoxData.height;
    var width = imageData.width,
        height = imageData.height;

    var left = convertExponentialToDecimal(cropBoxData.left - canvasData.left - imageData.left);
    var top = convertExponentialToDecimal(cropBoxData.top - canvasData.top - imageData.top);

    var imgPreivewElement = $('#preview-image' + boxIndex + '_3').children()[0];

    imgPreivewElement.style.display = "inline-block";
    imgPreivewElement.src = $("#imgModal").attr("src");
    imgPreivewElement.style.cssText = 'display:block;' + 'width:100%;' + 'height:auto;' + 'min-width:0!important;' + 'min-height:0!important;' + 'max-width:none!important;' + 'max-height:none!important;' + 'image-orientation:0deg!important;"';



    var data = $('#preview-image' + boxIndex + '_3');
    // console.log(data);
    var originalWidth = data.width();
    var originalHeight = data.height();
    var newWidth = originalWidth;
    var newHeight = originalHeight;
    var ratio = 1;

    if (cropBoxWidth) {
        ratio = originalWidth / cropBoxWidth;
        newHeight = cropBoxHeight * ratio;
    }

    if (cropBoxHeight && newHeight > originalHeight) {
        ratio = originalHeight / cropBoxHeight;
        newWidth = cropBoxWidth * ratio;
        newHeight = originalHeight;
    }


    $('#preview-image' + boxIndex + '_3').css({
        'width': newWidth + 'px',
        // 'height': newHeight + 'px'
    });

    $('#preview-image' + boxIndex + '_3').children().css({
        'width': width * ratio + 'px',
        'height': height * ratio + 'px',
        'transform': `translateX(${-left * ratio}px) translateY(-${top * ratio}px)`,
        'border-radius': '0'
    });

    // calculations for preview page
    originalWidth = window.innerWidth;
    originalHeight = 500;
    newWidth = originalWidth;
    newHeight = originalHeight;
    ratio = 1;

    if (cropBoxWidth) {
        ratio = originalWidth / cropBoxWidth;
        newHeight = cropBoxHeight * ratio;
    }

    if (cropBoxHeight && newHeight > originalHeight) {
        ratio = originalHeight / cropBoxHeight;
        newWidth = cropBoxWidth * ratio;
        newHeight = originalHeight;
    }

    var cssText = 'display:block;' + `width: ${width * ratio}px; ` + `height: ${height * ratio}px; ` + `transform: translateX(${-left * ratio}px) translateY(-${top * ratio}px);` + 'min-width:0!important;' + 'min-height:0!important;' + 'max-width:none!important;' + 'max-height:none!important;' + 'image-orientation:0deg!important;"';
    // console.log({'width': width * ratio + 'px','height': height * ratio + 'px'})
    // console.log(`translateX(${-left * ratio}px) translateY(-${top * ratio}px)`);
    // cssText += ;
    return cssText;
}


$('#labImgModal').on('shown.bs.modal', function () {
    console.log("working");
    let imgElement = document.getElementById("imgModal");
    if (cropper) {
        cropper.destroy();
    }
    cropper = new Cropper(imgElement, {
        viewMode: 1,
        aspectRatio: 2.69,
    })
}).on("hidden.bs.modal", function () {
    // myPreview(cropper, lab_img_for);
});

$('.lab-research-list .lab_research_image').each(function () {
    $(this).on("change", function () {
        if (this.files && this.files[0]) {
            let reader = new FileReader();
            reader.onload = function (e) {
                $("#imgModal").attr("src", e.target.result);
                $("#labImgModal").modal("show");
            }
            reader.readAsDataURL(this.files[0]);
            console.log($(this).attr('name'));
            lab_img_for = $(this).attr('name');
        }
    });
});


$("#labImgCropBtn").click(function () {
    newInputs = $(".new-hidden-input-box").html();
    var cropData = cropper.getData();

    if ($(".cropping-hidden-input-box").find("#x_" + lab_img_for).length) {

        $(".cropping-hidden-input-box").find("#x_" + lab_img_for).attr({
            value: cropData['x']
        });
        $(".cropping-hidden-input-box").find("#y_" + lab_img_for).attr({
            value: cropData['y']
        });
        $(".cropping-hidden-input-box").find("#w_" + lab_img_for).attr({
            value: cropData['width']
        });
        $(".cropping-hidden-input-box").find("#h_" + lab_img_for).attr({
            value: cropData['height']
        });
    } else {

        $(".cropping-hidden-input-box").append(newInputs);

        $(".cropping-hidden-input-box").children().last().children().first().attr({
            name: "x_" + lab_img_for,
            id: "x_" + lab_img_for,
            value: cropData['x'],
        });
        $(".cropping-hidden-input-box").children().last().children().eq(1).attr({
            name: "y_" + lab_img_for,
            id: "y_" + lab_img_for,
            value: cropData['y']
        });
        $(".cropping-hidden-input-box").children().last().children().eq(2).attr({
            name: "w_" + lab_img_for,
            id: "w_" + lab_img_for,
            value: cropData['width']
        });
        $(".cropping-hidden-input-box").children().last().children().eq(3).attr({
            name: "h_" + lab_img_for,
            id: "h_" + lab_img_for,
            value: cropData['height']
        });
        $(".cropping-hidden-input-box").children().last().children().eq(4).attr({
            name: lab_img_for + "-cssText",
            id: lab_img_for + "-cssText"
        });
    }


    var cssText = labPreview(cropper, lab_img_for);
    $(".cropping-hidden-input-box").find('#' + lab_img_for + "-cssText").val(cssText);

    $("#labImgModal").modal("hide");
});


$('#upload-project-photo, #addi-image-1, #addi-image-2, #addi-image-3').change(function () {
    if (this.files && this.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $("#imgModal").attr("src", e.target.result);
            $("#divModal").modal("show");
        }
        reader.readAsDataURL(this.files[0]);
        // console.log($(this).attr('name'));
        picture_for = $(this).attr('name');
    }

});

$('#divModal').on('shown.bs.modal', function () {
    let imgElement = document.getElementById('imgModal');
    if (cropper) {
        cropper.destroy()
    };
    var ratio = 1.285;
    if(picture_for !== 'banner'){
        var previewBox = $('.img-preview-' + picture_for);
        ratio = previewBox.width()/previewBox.height();
        // console.log(ratio);
    }
    cropper = new Cropper(imgElement, {
        viewMode: 1,
        aspectRatio: ratio, // non banner pic only 
        // preview: '.img-preview-'+picture_for
    })
    if (picture_for === 'banner') {
        cropper.setAspectRatio(22.6 / 9);
    }

    // console.log('.img-preview-'+ picture_for);
}).on("hidden.bs.modal", function () {
    $(`[name=${picture_for}]`).val('');
});

function newCropPreview(){
    var final_imgurl =  cropper.getCroppedCanvas().toDataURL();
    console.log("final_imgurl",final_imgurl)
    if(jQuery("input[name='is_edit']").length == 1) {
        jQuery(".img-preview-banner").css('background-image','none')
    }
    $("#img-final-banner").attr("src",final_imgurl).width($("#parent_banner").width()).height(600).show();

}
$("#ImgCropBtn").click(() => {
    let cropData = cropper.getData();
    $("#x_" + picture_for).val(cropData['x']);
    $("#y_" + picture_for).val(cropData['y']);
    $("#w_" + picture_for).val(cropData['width']);
    $("#h_" + picture_for).val(cropData['height']);
    if(picture_for =="banner"){
        newCropPreview(cropper);
    }else{
        myPreview(cropper, picture_for);
    }
    
    $("#divModal").modal("hide");
});


$( window ).resize(function() {
  $("#img-final-banner").width($("#parent_banner").width()).height(600).show();
});
$('#author-img-input').change(function () {
    if (this.files && this.files[0]) {
        let reader = new FileReader();
        reader.onload = function (e) {
            $("#profileModalImg").attr("src", e.target.result);
            $("#profileModal").modal("show");
        }
        reader.readAsDataURL(this.files[0]);
    }
    $(".author-img-view").css({ 'background-image': '' });
});

$('#profileModal').on('shown.bs.modal', function () {
    let imgElement = document.getElementById('profileModalImg');
    if (cropper) {
        cropper.destroy();
    }
    cropper = new Cropper(imgElement, {
        viewMode: 1,
        aspectRatio: 1 / 1,
        preview: '.author-img-view'

    });
    // for preview
    setTimeout(() => {
        $(".author-img-view").css({ 'background-image': '' });
        $(".author-img-view").children().css({ 'border-radius': '0' });
    }, 500);

}).on("hidden.bs.modal", function () {
    // cropper.destroy();
});

$("#profileCropBtn").click(() => {
    let cropData = cropper.getData();
    $("#x_profile").val(cropData['x']);
    $("#y_profile").val(cropData['y']);
    $("#w_profile").val(cropData['width']);
    $("#h_profile").val(cropData['height']);
    $("#profileModal").modal("hide");
});

$("#img-crop-zoom-out").click(function () {
    cropper.zoom(-0.1);
});
$("#img-crop-zoom-in").click(function () {
    cropper.zoom(0.1);
});

$('#img-crop-left').click(() => {
    cropper.move(-10, 0);
});
$('#img-crop-right').click(() => {
    cropper.move(10, 0);
});
$('#img-crop-up').click(() => {
    cropper.move(0, 10);
});
$('#img-crop-down').click(() => {
    cropper.move(0, -10);
});