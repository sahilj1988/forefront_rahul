var HOST = window.location.origin+"/"


var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
};


$(document).ready(function() {
    var search_param = getUrlParameter('q')
    if(typeof(search_param) == 'undefined'){
        search_param = ''
    }
    var project_table = $('#project_table').DataTable( {
        "processing": true,
        "serverSide": true,
        "pageLength": 5,
        "searching": false,
        "info": false,
        "ordering": false,
        "ajax": {
            "url": HOST+"search-paginate-projects/"+search_param,
            "dataSrc": function ( json ) {
                //Make your callback here.
                // $(".dataTables_processing").hide()
                $(".dataTables_length label").hide()
                jQuery(".search-top-container").eq(1).html("<div class='results-count'>"+json.recordsTotal+" results found.</div>")
                console.log(json.data)
                return json.data;
            },
        },
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    var html_tag = '<div class="col-md-10">'+
        
                                        '<p><a class="redirect_url" href="'+ row.project_anchor_url+'">'+ row.name+'</a></p>'+
                                        
                                        '<p class="search_item"></p>'+
                                        
                                        
                                        '<p></p><p></p><p></p><p></p><p></p><p></p>'+
                                        '<p><a class="redirect_url prof_redirect_url" href="'+row.professor_anchor_url+'"> '+row.professor_first_name+' '+row.professor_last_name+' (Professor)</a></p>'+
                                        
                                        '<p></p><p></p>'
                    if(typeof(row.owner_first_name)!='undefined'){
                        html_tag = html_tag+ '<p><a class="redirect_url prof_redirect_url" href="'+row.owner_anchor_url+'"> '+row.owner_first_name+' '+row.owner_last_name+' (Owner)</a></p>'
                    
                    }
                    return html_tag+'</div>';

                },
                "targets": 0
            }
        ]
    } );
    $('#groups_table').DataTable( {
        "processing": true,
        "serverSide": true,
        "searching": false,
        "info": false,
        "pageLength": 5,
        "ordering": false,
        "ajax": {
            "url": HOST+"search-paginate-groups/"+search_param,
            "dataSrc": function ( json ) {
                //Make your callback here.
                $(".dataTables_processing").hide()
                $(".dataTables_length label").hide()
                jQuery(".search-top-container").eq(0).html("<div class='results-count'>"+json.recordsTotal+" results found.</div>")                
                return json.data;
            },
        },
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    var html = '<div class="col-md-10">'+
        
                                    '<p><a href="'+row.lab_anchor_url+'" class="redirect_url">'+ row.name+'</a></p>'+
                                
                                '<p class="search_item"><a href="'+row.owner_anchor_url+'" class="redirect_url">'+ row.owner_first_name+' '+ row.owner_last_name+' (Lab Admin)</a></p>'+
                                '<p class="search_item"><a href="'+row.professor_anchor_url+'" class="redirect_url prof_redirect_url">'+ row.professor_first_name+' '+ row.professor_last_name+' (Professor)</a></p>'+
                                '<p class="search_term">'+ row.university_name+'</p>'+
                            '</div>';
                    return html;
                },
                "targets": 0
            }
        ]
    } );
    $('#members_table').DataTable( {
        "processing": true,
        "serverSide": true,
        "searching": false,
        "info": false,
        "pageLength": 5,
        "ordering": false,
        "ajax": {
            "url": HOST+"search-paginate-members/"+search_param,
            "dataSrc": function ( json ) {
                //Make your callback here.
                $(".dataTables_processing").hide()
                $(".dataTables_length label").hide()
                jQuery(".search-top-container").eq(2).html("<div class='results-count'>"+json.recordsTotal+" results found.</div>")
                return json.data;
            },
        },
        "columnDefs": [
            {
                "render": function ( data, type, row ) {
                    var html = '<div class="row result">'+
                                    '<div class="col-md-2 col-sm-3 col-xs-12" style="padding: 0 0; margin-left:auto; margin-right: auto;">'+
                                        '<a href="'+row.member_anchor_url+'"><img src="'+row.member_img_url+'" alt="Test2" class="thumbnail-image thumbnail_img_style"></a>'+
                                    '</div>'+
                                    '<div class="col-md-10 col-sm-9 col-xs-12">'+

                                        '<p class="search_term"><a class="redirect_url" href="'+row.member_anchor_url+'">'+row.first_name+' '+row.last_name+'</a></p>'+
                                        '<p class="search_term">'+row.university_name+'</p>'+
                                    '</div>'+
                                '</div>'
                    return html;
                },
                "targets": 0
            }
        ]
    } );
} );


