from django	import template
from ..models import Project, Member

register = template.Library()

@register.simple_tag 
def	get_comment(member_id, project_id):
    m = Member.objects.filter(id=member_id)[0]
    return m.memberprojectcomment_set.all().filter(project_id=project_id)[0].comment