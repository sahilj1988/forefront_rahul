from dal import autocomplete,forward
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django import forms
from .models import Member, University, MemberProjectComment, Project
from random import choice
from string import ascii_lowercase, digits
from django.conf import settings
from django.db.models import Q
import re
# ModelSelect2Multiple
class AuthorForm(forms.ModelForm):
    author_select = forms.ModelChoiceField(
        queryset=Member.objects.filter(Q(user__type='SU')| Q(user__type='PF')),
        widget=autocomplete.ModelSelect2Multiple(url='authors-autocomplete', attrs={'data-html': True}, forward=None),
    )
    authors = forms.ModelChoiceField(
        queryset=Member.objects.filter(Q(user__type='SU')| Q(user__type='PF'))
    )
    class Meta:
        model = Member
        fields = ('__all__')


class ProjectForm(forms.ModelForm):
    author_select = forms.ModelChoiceField(
        queryset=Member.objects.filter(user__type='SU'),
        widget=autocomplete.ModelSelect2Multiple(url='members-autocomplete', attrs={'data-html': True}),
    )
    authors = forms.ModelChoiceField(
        queryset=Member.objects.filter(user__type='SU')
    )
    class Meta:
        model = Member
        fields = ('__all__')

class UniversityForm(forms.ModelForm):
    university_select = forms.ModelChoiceField(
        queryset=University.objects.all(),
        widget=autocomplete.ModelSelect2(url='university-autocomplete', attrs={'data-html': True}),
    )
    universities = forms.ModelChoiceField(
        queryset=University.objects.all()
    )
    class Meta:
        model = University
        fields = ('__all__')


class ProfessorForm(forms.ModelForm):
    prof_select = forms.ModelChoiceField(
        queryset=Member.objects.filter(user__type='PF'),
        widget=autocomplete.ModelSelect2Multiple(url='professors-autocomplete', attrs={'data-html': True}, forward=None),
    )
    prof = forms.ModelChoiceField(
        queryset=Member.objects.filter(user__type='PF')
    )
    class Meta:
        model = Member
        fields = ('__all__')

class SignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2',)

def generate_random_username(length=16, chars=ascii_lowercase+digits, split=4, delimiter='-'):

    username = ''.join([choice(chars) for i in range(length)])

    if split:
        username = delimiter.join([username[start:start+split] for start in range(0, len(username), split)])

    try:
        User.objects.get(username=username)
        return generate_random_username(length=length, chars=chars, split=split, delimiter=delimiter)
    except User.DoesNotExist:
        return username;

class SignUpForm2(UserCreationForm):
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2',)
    
    def clean_email(self):
        email = self.cleaned_data.get('email')
        if not settings.DEBUG:
            # regex = '^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*\.(edu)$'
            regex = r'[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.(edu)'
            if re.search(regex,email) :
                print("Valid Email")
                return email
            else:
                print("Invalid")
                raise forms.ValidationError("Please, Enter an .edu email!")
        else:            
            return email

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.username = generate_random_username()
        user.email = self.cleaned_data["email"]

        if commit:
            user.save()

        return user


# class ProductForm(ModelForm):
#     class Meta:
#         model = Product
#         exclude = ('updated', 'created')

#     def __init__(self, args, *kwargs):
#         super(ProductForm, self).__init__(*args, **kwargs)
#         self.fields['description'].widget = TextInput(attrs={
#             'id': 'myCustomId',
#             'class': 'myCustomClass',
#             'name': 'myCustomName',
#             'placeholder': 'myCustomPlaceholder'})


class LabAdminUserForm(forms.Form):
    lab_name    = forms.CharField(label="Lab Name", max_length=255)
    university  = forms.ModelChoiceField(label="University", 
                    queryset=University.objects.order_by('name'), 
                    empty_label="---------------------"
                )

class UserInvitationForm(forms.Form):
    email = forms.EmailField(label="Email",widget=forms.TextInput(attrs={'placeholder': 'Email'}), required=True)
    projects = forms.ChoiceField(required=True)    

    class Meta:
        fields = ('email', 'projects')

    def clean_email(self):
        print('herere1')
        email = self.cleaned_data['email']
        is_email_exist = User.objects.filter(email=email)
        if is_email_exist:
            print('ddd')
            raise forms.ValidationError('Email already exists.')
        if email:
            raise forms.ValidationError('Please check email field.')
        return email

    def clean_projects(self):
        project_id = self.cleaned_data['projects']
        print('project_id')
        print(project_id)
        is_project_exist = Project.objects.filter(id=project_id)
        if not is_project_exist:
            raise forms.ValidationError('Selected Project does not exist..')
        if project_id:
            raise forms.ValidationError('Please check projects field.')
        return project_id    



    def __init__(self, request, *args, **kwargs):
        super().__init__(*args, **kwargs)
        member_id = request.user.customuser.member.id
        project_ids = list(MemberProjectComment.objects.filter(member_id=member_id).values_list('project_id', flat=True))
        projects_arr = list(Project.objects.filter(id__in=project_ids).values_list('id','name'))
        projects_arr.insert(0, [None,'Select Project'])
        self.fields['projects'].choices=projects_arr

