from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.utils.safestring import mark_safe
from django.contrib.auth import logout  
from urllib.parse import urlparse
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.views.generic import TemplateView, CreateView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.contrib.postgres.search import SearchVector
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, get_user_model
from django.http.response import HttpResponseRedirect
from projects.forms import SignUpForm2
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from projects.tokens import account_activation_token
from django.contrib.sites.shortcuts import get_current_site
from django.views import View
from django.http import JsonResponse, HttpResponse
import smtplib
from django.conf import settings
from email.mime.text import MIMEText
from email.header import Header
from email.utils import formataddr
from django.template.loader import render_to_string
import base64
from projects.models import Member, CustomUser, Lab, University, LabResearchTopic, LabPublication
import string
from dal import autocomplete
from django.db.models import Q
import random
from django.contrib.auth.hashers import check_password

from django.core.paginator import Paginator


from .forms import ProjectForm, ProfessorForm, AuthorForm, UniversityForm, UserInvitationForm
from .models import Project, Member, Lab, Userlikes, University, MemberQualification, MemberWorkExperience, MemberProjectComment
import os
import json
from .s3_handler import S3Handler
import time
from django.core.files.storage import FileSystemStorage
from django.utils.html import format_html
# from django.conf import settings.AWS_S3_CUSTOM_DOMAIN
from projects.forms import LabAdminUserForm

from PIL import Image
from itertools import chain
import pdb

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib import messages
from .utilities import Utilities



num_records_per_page = 3
User = get_user_model()



def csrf_failure(request, reason=""):
    ctx = {'msg': 'Sorry, We are unable to log you In. Please First logout then log In.'}
    return render_to_response('csrf_fail.html', ctx)

class HomeView(UserPassesTestMixin, ListView):
    model = Project
    template_name="projects/index.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['class_projects_page'] = "active"
        likes = self.login_user_like(self.object_list)
        context['zipped'] = zip(self.object_list,likes)      
        return context
 
    def login_user_like(self, object_list):
        like_status = []
        project_details = list(object_list.values())
        # check user is login or not

        if self.request.user.is_authenticated:
            # get project id from Project model and then compare project id and present user into Userlikes model.
            for project in project_details:
                user_likes = list(Userlikes.objects.all().filter(user_id=self.request.user.id, project_id = project['id']).values())

                if user_likes:
                    if user_likes[0]['like'] == 1:
                        like_status.append(1)
                    else:
                        if user_likes[0]['like'] == 0:
                            like_status.append(0)
                else:
                    like_status.append(0)

            return like_status
        else:
            for detail in project_details:
                like_status.append(0)
            return like_status
    
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

class LabsView(UserPassesTestMixin, ListView):
    model = Lab
    template_name="projects/labs.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['class_labs_page'] = "active"
        return context
    
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
            # return redirect('login')
        return True

class LabView(UserPassesTestMixin, View):
    def get(self, request, pk = None, **kwargs):
        lab = Lab.objects.filter(id=pk)[0]
        filtered_members = lab.members.filter(user__type='SU')
        lab.members.set(filtered_members)
        # admin_detail = Member.objects.filter(user_id=lab.id).values()[0]
        admin_detail = Member.objects.filter(user_id=lab.owner_id)[0]
        return render(request, "projects/lab.html", {'object':lab, 'admin_detail': admin_detail})

    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
            # return redirect('login')
        return True




class AuthorCreate(UserPassesTestMixin, TemplateView):
    template_name = 'projects/author_create.html'
        
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
            # return redirect('login')
        return True



class AuthorView(UserPassesTestMixin, DetailView):
    model = Member
    template_name="projects/author.html"
    def get_context_data(self, **kwargs):
        lab_id = None
        context = super().get_context_data(**kwargs)
        context['qualification']=MemberQualification.objects.filter(member_id=self.object.id)
        context['experience'] = MemberWorkExperience.objects.filter(member_id=self.object.id)
        labs_arr = []
        labs = Lab.objects.all()
        check_member = None
        if labs:
            # for single_lab in labs:
            for lab in labs:
                if self.object.user.type == 'SU':
                    check_member = lab.members.filter(id=self.object.id)
                else:
                    if self.object.user.type == 'PF':
                        check_member = lab.professors.filter(id=self.object.id)
                if check_member:
                    labs_arr.append({'name':lab.name, 'lab_id': lab.id})
        context['labs_arr'] = labs_arr
        likes = self.login_user_like(self.object.project_set.all())
        context['zipped'] = zip(self.object.project_set.all(),likes)
        # if current user type is professor, then assign lab_id otherwise assign none
        if self.object.user.type == 'PF':
            list_labs = list(Lab.objects.all())
            for single_lab in list_labs:
                professor_list = list(single_lab.professors.all())
                for single_professor in professor_list:
                    if single_professor.id == self.object.id:
                        lab_id = single_lab.id
                        break
            
        if lab_id:
            context['lab_id'] = lab_id
        else:
            context['lab_id'] = None
        return context

    def login_user_like(self, object_list):
        like_status = []
        project_details = list(object_list.values())
        # check user is login or not

        if self.request.user.is_authenticated:
            # get project id from Project model and then compare project id and present user into Userlikes model.
            for project in project_details:
                user_likes = list(Userlikes.objects.all().filter(user_id=self.request.user.id, project_id = project['id']).values())

                if user_likes:
                    if user_likes[0]['like'] == 1:
                        like_status.append(1)
                    else:
                        if user_likes[0]['like'] == 0:
                            like_status.append(0)
                else:
                    like_status.append(0)

            return like_status
        else:
            for detail in project_details:
                like_status.append(0)
            return like_status
        
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True


class ProjectView(UserPassesTestMixin, DetailView):
    model = Project
    template_name="projects/project.html"
    def get_context_data(self, **kwargs):
        authors_arr = []
        authors_commented_arr = []
        context = super().get_context_data(**kwargs)
        authors_commented_arr = list(MemberProjectComment.objects.filter(project_id=self.object.id).all().values())

        # preparing data to be shown authors on project page.
        for author in authors_commented_arr:
            mem = Member.objects.filter(id=author['member_id']).values()[0]
            mem['contribution'] = author['comment']
            mem['image_url']='https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+mem['image'] if mem['image'] else None
            authors_arr.append(mem)
        context['authors_arr'] = authors_arr
        context['project_page'] = 'project page'


        return context
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

class LoginView(auth_views.LoginView):
    template_name="projects/login.html"


    def post(self, request):
        _next_post = request.POST.get('next')
        user_email = request.POST.get('email').strip() if request.POST.get('email') else None
        user_pwd = request.POST.get('password').strip() if request.POST.get('password') else None
        err_msg = None
        if user_email is not None and user_pwd is not None:
            # have a bug not checking whether the cridentail are correct or not present is db or not
            check_username = User.objects.filter(email=user_email)
            if check_username:
                check_username = User.objects.filter(email=user_email).values()[0]
                print('check_username')
                print(check_username)
                print(check_username['username'])
                if check_password(user_pwd, check_username['password']):
                    print('pwd')
                    print(check_password(user_pwd, check_username['password']))
                    aut = authenticate(username=check_username['username'],password=user_pwd)
                    try:
                        print('aut')
                        print(aut)
                        cuser = aut.customuser
                        if cuser.type == 'LA' and cuser.status == 0:
                            # send message wait to admins to approve
                            # lab admin user not approved!
                            pass
                        else:
                            login(request, aut)
                            if _next_post:
                                return redirect(_next_post)
                            if cuser.type == 'LA':
                                return redirect('labs')
                            elif cuser.type == 'PF':
                                if cuser.member.first_name == '' or cuser.member.last_name == '':
                                    return redirect('profile')
                                return redirect('author', pk=cuser.member.pk)
                            else:
                                return redirect('home')
                    except:
                        login(request, aut)
                        if _next_post:
                            return redirect(_next_post)
                        return redirect('home')

                    return redirect('home')
                else:
                    return render(request, 'projects/login.html',{"err_msg":"Your username and password didn't match. Please try again."})
            else:
                return render(request, 'projects/login.html',{"err_msg":"Your username and password didn't match. Please try again."})
        else:
            return render(request, 'projects/login.html',{"err_msg":"Fields are empty."})  # applied js validation.



class LogoutView(auth_views.LogoutView):
    pass

class SignupView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('profile')
    template_name = 'projects/index.html'
    def form_valid(self, form):

        if form.is_valid():
            new_user = form.save()
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(self.request, new_user)
            return HttpResponseRedirect(self.success_url)
        else:
            return HttpResponseRedirect(reverse_lazy('home'))

def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')

def activate(request, uidb64, token):

    try:
        # Decode the encoded UID of the user
        uid = urlsafe_base64_decode(uidb64).decode()
        # Get the user with the given UID
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        # If UID is not valid
        user = None
        print("[LOG] User not existant ???")

    if user is not None and account_activation_token.check_token(user, token):

        # Activate User
        user.is_active = True
        user.save()             # Save changes to model

        # Get the CustomUser for the given user
        cuser = CustomUser.objects.get(user=user)
        # If user is not valid
        if cuser is None:
            return render(request, 'projects/account_activation_invalid.html')

        # Check if the user is Lab Admin
        if cuser.type == 'LA':
            cuser.status = 0        # Default disable if lab admin
        else:
            cuser.status = 1        # Enable if Student

        # Save the CustomUser
        cuser.save()

        # Print the email

        # Create a Member from user
        # member = Member(user=cuser)
        # member.email = user.email
        # member.save()

        if cuser.type != 'LA':
            print('activate func----',cuser.type)
        else:
            # send mail to Admin
            subject = 'We Have New Lab Admin User Sign Up!(Need Action)'
            try:
                admin = User.objects.filter(is_active=True, is_superuser=True)[0]
                admin_email = admin.email
                if not admin_email:
                    print('Admin does not have any email associated with it!')
            except IndexError as e:
                print('Superuser doesnt exists! Please Create One',e)
                # return HttpResponseRedirect
            
            current_site = get_current_site(request)  
            message = render_to_string('projects/new_lab_admin_approval.html',
            {
                'admin': admin,
                'domain': current_site.domain,
            })
            # mail_user( subject, message, admin_email)
            Utilities().email_user(subject=subject, message=message, to_email=admin_email)
            return render(request, 'projects/account_activation_lab_admin_wait.html')

        return render(request, 'projects/account_activation_success.html')
    else:
        print("[LOG] Check token fail ???")
        return render(request, 'projects/account_activation_invalid.html')

# def mail_user(subject, message, end_user,heading="verify email"):
#     msg = MIMEText(message, 'html', 'utf-8')    # i replaced html in place of plain
#     msg['Subject'] = Header(subject, 'utf-8')
#     msg['From'] = formataddr((str(Header(heading, 'utf-8')), settings.EMAIL_USER))
#     msg['To'] = end_user


#     server = smtplib.SMTP_SSL(settings.EMAIL_HOST, 465)
#     server.login(settings.EMAIL_USER, settings.EMAIL_PASSWORD)
#     server.sendmail(settings.EMAIL_USER, [end_user], msg.as_string())
#     server.quit()

def signup(request):
    if request.method == 'POST':

        # Get the "type" of the user
        type = request.POST.get("user_type")

        if type is None:
            err = "User Type not selected"
            return render(request, 'projects/signup.html', {'issue':err+str("<br> Signup again. <a href='/'>Here</a>")})

        form = SignUpForm2(request.POST)

        # If the given form is valid
        if form.is_valid():

            # Create user using the form details
            user = form.save(commit=False)

            # Login details
            # Check if user exists with email
            try:
                alt_user = User.objects.get(email=user.email)
                err = "Email already in use"
                return render(request, 'projects/signup.html', {'issue':err+str("<br> Signup again.")})
            except:
                print("[LOG] No issue with new user setup")
            
            user.set_password(form.cleaned_data.get('password1'))
            user.is_active = False                   # Disable the user
            user.save()

            """
            # Create a record in CustomUser
            """
            cuser = CustomUser(user=user, type=type)

            cuser.save()    # Save the record

            ##
            # Email details
            ##
            current_site = get_current_site(request)           # Domain
            subject = 'Activate Your Forefront Account'   # Subject of the email
            # Base64 UID of the user
            uid = urlsafe_base64_encode(force_bytes(user.pk))
            # Verification token
            token = account_activation_token.make_token(user)

            """
            print("[LOG] UID: ")
            print(uid)
            print("[LOG] Token: ")
            print(token)
            """
            parsed_uri = urlparse(request.build_absolute_uri())
            base_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
            
            message = render_to_string('projects/account_activation_email.html',
            {
                'user': user,
                'domain': base_url,
                'uid': uid,
                'token': token,
            })

            # mail_user( subject, message, user.email )
            Utilities().email_user(subject=subject, message=message, to_email=user.email)
            return render(request, 'projects/account_activation_sent.html')
        else:
            return render(request, 'projects/signup.html', {'issue':str(form.errors)+str("<br> Signup again. <a href='/'>Here</a>")})
    else:
        form = SignUpForm2()
        return render(request, 'projects/login.html', {'form':form})

class ProjectEditView(UserPassesTestMixin, View):
    def get(self, request, id = None, **kwargs):
        if id:
            comments_arr = []
            
            project_detail = get_object_or_404(Project, id=id)
            if project_detail:

                if self.request.user.is_authenticated:
                    members = Member.objects.all().exclude(first_name__isnull=True, last_name__isnull=True, image__isnull=True, position__isnull=True, ideal_role__isnull=True, website__isnull=True).exclude(first_name__exact='', last_name__exact='')
                else:
                    current_user = None
                    members = Member.objects.none()
                for author in project_detail.authors.all():
                    comment_data = MemberProjectComment.objects.filter(member_id = author.id, project_id = project_detail.id)

                # if 'comments_arr' in self.request.session:
                #     del self.request.session['comments_arr']

                # if 'authors_arr' in self.request.session:
                #     del self.request.session['authors_arr']

                # put comments and their authors into session on loading of this view, they will be later used in by clicking on preview button.
                if 'is_preview_on_edit' in self.request.session and 'previewed_project_id' in self.request.session and self.request.session['previewed_project_id'] != None:
                    if int(self.request.session['previewed_project_id']) != id:
                        if 'comments_arr' in self.request.session:
                            del self.request.session['comments_arr']

                        if 'authors_arr' in self.request.session:
                            del self.request.session['authors_arr']                    

                        if 'is_preview_on_edit' in self.request.session:
                            del self.request.session['is_preview_on_edit']

                        if 'previewed_project_id' in self.request.session:
                            del self.request.session['previewed_project_id']                  

                        member_comments_query = MemberProjectComment.objects.filter(project_id=id)
                        for member_comment in member_comments_query:
                            image = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(member_comment.member.image)
                            position = member_comment.member.position
                            comments_arr.append({'project_member': member_comment.member.id, 'comment': member_comment.comment, 'position':position, 'image': str(image) })
                        self.request.session['comments_arr']= json.dumps(comments_arr)


                        # member_comments_query = list(MemberProjectComment.objects.filter(project_id=id).all().values())
                        # current_project_members = project_detail.authors.all()
                        # if member_comments_query:
                        #     for member_comment in member_comments_query:
                                
                        #         image = ['https://'+AWS_S3_CUSTOM_DOMAIN+'/'+str(single_member.image) for single_member in current_project_members if single_member.id == int(member_comment['member_id'])][0]
                        #         position = [single_member.position for single_member in current_project_members if single_member.id == int(member_comment['member_id'])][0]

                        #         comments_arr.append({'project_member': member_comment['member_id'], 'comment': member_comment['comment'], 'position':position, 'image': str(image) })
                        #     self.request.session['comments_arr']= json.dumps(comments_arr)
                        
                    pass
                else:
                    if 'comments_arr' in self.request.session:
                        del self.request.session['comments_arr']

                    if 'authors_arr' in self.request.session:
                        del self.request.session['authors_arr']                    

                    if 'is_preview_on_edit' in self.request.session:
                        del self.request.session['is_preview_on_edit']
                    
                    if 'previewed_project_id' in self.request.session:
                        del self.request.session['previewed_project_id']



                    member_comments_query = MemberProjectComment.objects.filter(project_id=id)
                    for member_comment in member_comments_query:
                        image = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(member_comment.member.image)
                        position = member_comment.member.position
                        comments_arr.append({'project_member': member_comment.member.id, 'comment': member_comment.comment, 'position':position, 'image': str(image) })
                    self.request.session['comments_arr']= json.dumps(comments_arr)



                    # member_comments_query = list(MemberProjectComment.objects.filter(project_id=id).all().values())
                    # current_project_members = project_detail.authors.all()
                    # if member_comments_query:
                    #     for member_comment in member_comments_query:
                    #         image = ['https://'+AWS_S3_CUSTOM_DOMAIN+'/'+str(single_member.image) for single_member in current_project_members if single_member.id == int(member_comment['member_id'])][0]
                    #         position = [single_member.position for single_member in current_project_members if single_member.id == int(member_comment['member_id'])][0]

                    #         comments_arr.append({'project_member': member_comment['member_id'], 'comment': member_comment['comment'], 'position':position, 'image': str(image) })
                    #     self.request.session['comments_arr']= json.dumps(comments_arr)

                data = {'project_detail_edit': project_detail, 'form': AuthorForm(), 'members': members}
                return render(request, 'projects/upload.html', data)
        else:
            return redirect('home')
    
    def post(self, request, id, **kwargs):

        project = get_object_or_404(Project, id=id)
        if request.user.customuser.member in project.authors.all():
            # publish button clicked. save info into db.
            if request.POST.get('post_type') == 'project_upload':
                name = request.POST.get('name') if request.POST.get('name') else None
                excerpt = request.POST.get('excerpt') if request.POST.get('excerpt') else None
                body = request.POST.get('body') if request.POST.get('body') else None
                update_s3 = 0
                qs = MemberProjectComment.objects.filter(project_id=project.id, is_owner=0)
                for m in qs:
                    m.delete()
             
                if 'image' in request.FILES:
                    update_s3 =1
                    image = self.upload_file_locally(request.FILES.get('image'), 'banner')
                    Project.objects.filter(id=id).update(image=image)
                if 'image1' in request.FILES:
                    update_s3 =1
                    image1 = self.upload_file_locally(request.FILES.get('image1'), 'upm-img1')
                    Project.objects.filter(id=id).update(image1=image1)
                if 'image2' in request.FILES:
                    update_s3 =1
                    image2 = self.upload_file_locally(request.FILES.get('image2'), 'upm-img2')
                    Project.objects.filter(id=id).update(image2=image2)
                if 'image3' in request.FILES:
                    update_s3 =1
                    image3 = self.upload_file_locally(request.FILES.get('image3'), 'upm-img3')
                    Project.objects.filter(id=id).update(image3=image3)
                
                if update_s3:
                    self.upload_file_to_s3()

                # get all membercomments for that project and delete it
                qs = MemberProjectComment.objects.filter(project_id=project.id, is_owner=0)
                for m in qs:
                    m.delete()
                # delete all authors for that project
                project.authors.clear()

                authors_arr = json.loads(request.POST.get('authors_arr'))

                Project.objects.filter(id=id).update(name=name, excerpt=excerpt, body=body)
                # add all members 
                for author in authors_arr:
                    get_author = Member.objects.get(id=author['id'])
                    project.authors.add(get_author)
                
                self.save_comments(project.id)    # save all new and updated members comments
                return JsonResponse('Project Created Successfully.', safe=False)

            # click on submit button on contribution popup of project edit page.
            elif request.POST.get('post_type') == 'author_project_comment':
                status = 0
                comments = []
                comment = request.POST.get('comment')
                project_member = int(request.POST.get('project_member'))
                project_id = int(request.POST.get('project_id'))
                message = None
                owner_login_status = 0

                # if comments_arr session already exists, then check for already existing project member, if it already exists then update it otherwise push into an array.
                if 'comments_arr' in self.request.session:
                    sess_comments = json.loads(self.request.session['comments_arr'])
                    if len(sess_comments) > 1:
                        is_owner_login = MemberProjectComment.objects.filter(member_id=project_member, project_id=project_id, is_owner = 1).values()
                        if is_owner_login: 
                            if project_member == self.request.user.customuser.member.id:
                                for sess_comment in sess_comments:
                                    if sess_comment['project_member'] == project_member:
                                        status = 1
                                        sess_comment['comment'] = comment
                                        message = "Comment Posted Successfully."
                                        break
                            else:
                                status = 1
                                owner_login_status = 1
                                message = "You are not authorized to edit owner's comment."
                        else:
                            for sess_comment in sess_comments:
                                if sess_comment['project_member'] == project_member:
                                    status = 1
                                    sess_comment['comment'] = comment
                                    message = "Comment Posted Successfully."
                                    break

                        if status == 0:
                            sess_comments.append({'comment':comment, 'project_member':project_member})
                            message = "Comment Posted Successfully."
                        self.request.session['comments_arr'] = json.dumps(sess_comments)
                    else:
                        sess_comments.append({'comment':comment, 'project_member':project_member})
                        message = "Comment Posted Successfully."                        
                        self.request.session['comments_arr'] = json.dumps(sess_comments)



                return JsonResponse({'owner_login_status':owner_login_status, 'message': message, 'comments': comments})
            
            else:
                # save authors and their comments temporarily in session by clicking on preview button
                if request.POST.get('post_type') == 'author_save_temp':
                    authors_no_contribution = []
                    comments_arr = []
                    authors_arr = json.loads(request.POST.get('authors_arr'))

                    is_preview_on_edit = request.POST.get('is_preview_on_edit')
                    previewed_project_id = request.POST.get('previewed_project_id')
                    # upload page
                    if 'comments_arr' in self.request.session:
                        comments_arr = json.loads(self.request.session['comments_arr'])
                        # if other members comments exist, then we will be putting them in a new object authors_arr variable otherwise we need to generate error in response to again try.comments 
                        for author in authors_arr:
                            status = 0
                            for comment in comments_arr:
                                if int(author['id']) == 6:
                                    print(comment['project_member'])
                                if comment['project_member'] == int(author['id']):
                                    status = 1
                                    author['contribution'] = comment['comment']
                                    if 'image' in comment:
                                        author['image'] = comment['image']
                                    if 'position' in comment:
                                        author['position'] = comment['position']
                                else:
                                    pass

                            # if status == 0, that means comment member id doesn't matched with author id.
                            if status == 0:
                                mem = Member.objects.filter(id=author['id']).values()[0]
                                authors_no_contribution.append({'first_name':mem['first_name'], 'last_name':mem['last_name']})
                    else:
                        # if only login user exists but not other members
                        for author in authors_arr:
                            mem = Member.objects.filter(id=author['id']).values()[0]
                            authors_no_contribution.append({'first_name':mem['first_name'], 'last_name':mem['last_name']})

                    if len(authors_no_contribution) == 0:
                        self.request.session['is_preview_on_edit'] = is_preview_on_edit
                        self.request.session['previewed_project_id'] = previewed_project_id



                    self.request.session['authors_arr'] = json.dumps(authors_arr)
                    return JsonResponse({"authors_exist":authors_no_contribution, "comments_arr":self.request.session['comments_arr'] if 'comments_arr' in self.request.session else None})
        else:
            return JsonResponse("you are not authorized to edit this project", safe=False)
    
    def save_comments(self, project_id):
        authors_arr = []
        if 'authors_arr' in self.request.session:
            authors_arr = json.loads(self.request.session['authors_arr'])
            if len(authors_arr) > 0:
                owner_details = MemberProjectComment.objects.filter(project_id=project_id, is_owner=1)
                for author in authors_arr:
                    # if selected author is an owner and login to app
                    if owner_details[0].member_id == self.request.user.customuser.member.id and owner_details[0].member_id == int(author['id']):
                        MemberProjectComment.objects.filter(member_id=int(author['id']), project_id=project_id).update(comment = author['contribution'])
                    # if selected author is owner but he is not login to app.
                    elif owner_details[0].member_id != self.request.user.customuser.member.id and owner_details[0].member_id == int(author['id']):
                        pass
                    # added authors.
                    else:
                        MemberProjectComment(member_id=author['id'], project_id=project_id, comment = author['contribution']).save()

    def upload_file_to_s3(self):
        local_directory = 'media/'
        for root, dirs, files in os.walk(local_directory):
            for filename in files:
                local_path = os.path.join(root, filename)
                relative_path = os.path.relpath(local_path, local_directory)
                s3_path = os.path.join('uploads/project-photos',relative_path)
                s3_handler = S3Handler()
                s3_handler.upload_file(file_path=local_path, file_name=filename, s3_path= s3_path)
                os.remove('media/%s'%filename)

    def upload_file_locally(self, file, img_type):
        file_name = file.name.split(".")[0]+'_'+str(random.randint(9999,9999999))
        full_file_name = file_name+os.path.splitext(file.name)[1]
        fs = FileSystemStorage()
        saved_filename = fs.save(full_file_name,file) # fs.save(<file path with name>, file)
        filename = self.crop_image_and_save(saved_filename, img_type)
        return 'uploads/project-photos/'+filename

    def crop_image_and_save(self, filename, img_type):
        x = float(self.request.POST.get(f'x_{img_type}'))
        y = float(self.request.POST.get(f'y_{img_type}'))
        w = float(self.request.POST.get(f'w_{img_type}'))
        h = float(self.request.POST.get(f'h_{img_type}'))
        img = Image.open(f'media/{filename}')
        cropped_img = img.crop((x, y, w+x, h+y))
        # resized_img = cropped_img.resize((200, 200), Image.ANTIALIAS)
        cropped_img.save(f'media/{filename}')
        return filename
        
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

class ProfileView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    template_name="projects/profile.html"
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['years'] = ['',2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019]
        context['degrees'] = ['','MCA', 'BCA', 'BBA', 'MBA']
        context['universities'] = University.objects.all()

        member=list(Member.objects.filter(user_id=self.request.user.customuser.id).values())
        if not member:
            context['qualification'] = None
            context['work_exp'] = None
            context['object'] = None
            context['qualification_records_count'] = 1
            context['work_exp_count'] = 1
        else:
            context['qualification'] = MemberQualification.objects.filter(member_id=member[0]['id']).all() if MemberQualification.objects.filter(member_id=member[0]['id']).all() else None
            context['work_exp'] = MemberWorkExperience.objects.filter(member_id=member[0]['id']).all() if MemberWorkExperience.objects.filter(member_id=member[0]['id']).all() else None
            context['object'] = member[0]
            context['img_url'] = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+member[0]['image'] if member[0]['image'] else None
            context['qualification_records_count'] = MemberQualification.objects.filter(member_id=member[0]['id']).count() if MemberQualification.objects.filter(member_id=member[0]['id']).count() > 0 else 1
            context['work_exp_count'] = MemberWorkExperience.objects.filter(member_id=member[0]['id']).count() if MemberWorkExperience.objects.filter(member_id=member[0]['id']).count() > 0 else 1

        return context

    # submit button clicking on author profile page.
    def post(self, request, *args, **kwargs):
        if request.POST.get('post_type') == 'author_upload':
            latest_record = None
            first_name = request.POST.get('first_name') if request.POST.get('first_name') else ''
            last_name = request.POST.get('last_name') if request.POST.get('last_name') else ''
            position = request.POST.get('position') if request.POST.get('position') else ''
            institute = request.POST.get('institute') if request.POST.get('institute') else ''
            website = request.POST.get('website') if request.POST.get('website') else ''
            ideal_role = request.POST.get('ideal_role') if request.POST.get('ideal_role') else ''

            # get member id
            member=list(Member.objects.filter(user_id=request.user.customuser.id).values())
            if not member:
                member_insert = Member(first_name= first_name, user_id = request.user.customuser.id, last_name = last_name, position = position, university_id = institute, website= website, ideal_role=ideal_role)
                member_insert.save()
                latest_record = Member.objects.all().order_by('-id').values()[0]
            else:
                Member.objects.filter(id=member[0]['id']).update(first_name= first_name, last_name = last_name, position = position, university_id = institute, website= website, ideal_role=ideal_role)
                latest_record = member[0]
   
            if request.POST.get('is_edit'):
                MemberQualification.objects.filter(member_id=latest_record['id']).delete()
            edu_records_length = request.POST.get('edu_records_length')
            for single_count in range(int(edu_records_length)):
                uni_id = request.POST.get('uni'+str(single_count)+'_0')
                degree = request.POST.get('uni'+str(single_count)+'_1')
                year = request.POST.get('uni'+str(single_count)+'_2')
                if uni_id == None or degree == None or year == None:
                    continue
                else:
                    MemberQualification(member_id= latest_record['id'], university_id = uni_id,degree=degree, year=year).save()
            




            # save work experience records
            m = Member.objects.get(id=latest_record['id'])
            if not m.user.type == 'PF':    
                MemberWorkExperience.objects.filter(member_id=latest_record['id']).delete()
                exp_records_length = request.POST.get('exp_records_length')
                for single_count in range(int(exp_records_length)):
                    company = request.POST.get('exp'+str(single_count)+'_0') if request.POST.get('exp'+str(single_count)+'_0') else None
                    position = request.POST.get('exp'+str(single_count)+'_1') if request.POST.get('exp'+str(single_count)+'_1') else None
                    yearfrom = request.POST.get('exp'+str(single_count)+'_2') if request.POST.get('exp'+str(single_count)+'_2') else None
                    yearto = request.POST.get('exp'+str(single_count)+'_3') if request.POST.get('exp'+str(single_count)+'_3') else None
                    if company == None or position == None or yearfrom == None or yearto == None:
                        continue
                    else:
                        MemberWorkExperience(member_id= latest_record['id'], company = company,position=position, yearfrom=yearfrom, yearto=yearto).save()

            # save image
            if 'avatar' in request.FILES:
                file = request.FILES['avatar']
                fs = FileSystemStorage()
                saved_filename = fs.save(file.name,file) # fs.save(<file path with name>, file)
                x = float(request.POST.get('x_profile'))
                y = float(request.POST.get('y_profile'))
                w = float(request.POST.get('w_profile'))
                h = float(request.POST.get('h_profile'))

                with Image.open(f'media/{saved_filename}') as avatar:
                    avatar.crop((x, y, w+x, h+y)).save(f'media/{saved_filename}')
                
                update_id = Member.objects.get(id=latest_record['id'])
                update_id.image= 'uploads/profile-photos/'+saved_filename
                update_id.save()
                

                local_directory = 'media/'
                for root, dirs, files in os.walk(local_directory):
                    for filename in files:
                        local_path = os.path.join(root, filename)
                        relative_path = os.path.relpath(local_path, local_directory)
                        s3_path = os.path.join('uploads/profile-photos',relative_path)
                        s3_handler = S3Handler()
                        s3_handler.upload_file(file_path=local_path, file_name=filename, s3_path= s3_path)
                        os.remove('media/%s'%filename)

                



            # return JsonResponse('hhh', safe=False)
            return redirect('author', pk=latest_record['id'])
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

class SearchView(UserPassesTestMixin, ListView):
    template_name="projects/search.html"
    def get_queryset(self):
        # project view
        query = self.request.GET.get('q', '')
        page = self.request.GET.get('page', 1)
        if (query != ''):
            print("[LOG] Query came")
            object_list = Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt', 'body', 'authors', 'lab')).filter(search__icontains=query).distinct("pk")[0:num_records_per_page]

        else:
            object_list = Project.objects.all()[0:num_records_per_page]

        return object_list

    def get_groups_queryset(self):
        query = self.request.GET.get('q', '')
        page = self.request.GET.get('page', 1)

        object_list = Lab.objects.none()
        if (query != ''):
            print("[LOG] Query came")
            object_list = Lab.objects.annotate(search=SearchVector('name', 'professors__first_name', 'professors__last_name', 'university__name', 'excerpt', 'owner', 'members__first_name', 'members__last_name')).filter(search__icontains=query).distinct("pk")[0:num_records_per_page]
            print('groups data')
            print(object_list)
            print(list(object_list.values()))
            # paginator = Paginator(object_list, 5)
            # object_list = paginator.page(page)
        else:
            object_list = Lab.objects.all()[0:num_records_per_page]
        return object_list

    def get_members_queryset(self):
        query = self.request.GET.get('q', '')
        page = self.request.GET.get('page', 1)
        if (query != ''):
            arr_members_ids = []
            arr_project_members_ids = []
            arr_lab_professors_ids = []
            arr_lab_members_ids = []
            print("[LOG] Query came")
            arr_members_ids = list(Member.objects.annotate(search=SearchVector('first_name', 'last_name', 'position', 'ideal_role', 'education', 'website', 'university__name')).filter(search__icontains=query).values_list('id', flat=True))

            print(arr_members_ids)

            # get filtered list from project model
            object_list2 = Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt')).filter(search__icontains=query)
            
            
            for i in object_list2:
                # print(i.memberprojectcomment_set.values_list('id', flat=True))
                # arr_ids.append(list(i.memberprojectcomment_set.values_list('id', flat=True)))
                arr_project_members_ids += list(i.memberprojectcomment_set.values_list('member_id', flat=True))

            # get filtered list from lab model
            object_list3 = Lab.objects.annotate(search=SearchVector('name', 'excerpt', 'university__name', 'major_contributions', 'sources_of_funding')).filter(search__icontains=query)


            for i in object_list3:

                # assign lab professors ids into array
                for single_professor in i.professors.values():
                    # arr_lab_professors_ids += single_professor.id
                    arr_lab_professors_ids.append(single_professor['id'])

                # assign lab members ids into array
                for single_member in i.members.values():
                    arr_lab_members_ids.append(single_member['id'])


            all_members_ids = list(set(arr_members_ids + arr_project_members_ids + arr_lab_professors_ids + arr_lab_members_ids))
            print('all_members_ids')
            print(all_members_ids)
            object_list = Member.objects.filter(id__in=all_members_ids)[0:num_records_per_page]
            print(type(object_list))
            print(object_list)
            
            # paginator = Paginator(object_list, 5)
            # object_list = paginator.page(page)
        else:
            object_list = Member.objects.all()[0:num_records_per_page]

        return object_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #context['count'] = self.get_queryset().count()
        context['count'] = len(self.get_queryset())
        groups_list = self.get_groups_queryset()
        context['groups_list'] = groups_list
        #context['groups_count'] = groups_list.count()
        context['groups_count'] = len(groups_list)
        member_list = self.get_members_queryset()
        context['member_list'] = member_list
        #context['member_count'] = member_list.count()
        # context['member_count'] = len(member_list)

        # context['count'] = None
        # context['groups_list'] = None
        # context['member_list'] = None
        # context['groups_count'] = None
        # context['member_count'] = None

        return context
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

# search_paginate_json_view

# class SearchPaginationView(View):
@method_decorator(csrf_exempt)
def search_paginate_view(request):
    recordsTotal = None
    object_list = []

    if request.method == 'POST':
        print(request.body)
        import pdb; pdb.set_trace()
        # print(d['start'])

        search_data = request.POST.get('search[value]') if request.POST.get('search[value]') else None
        page_number_inputted = int(request.POST.get('start')) if request.POST.get('start') else None
        
        
        # json_data = json.loads(request.body)
        # print(json_data)
        # print(json_data['search[value]'])
        # print(json_data['start'])
        # search_data = json_data['search[value]']
        # page_number_inputted = json_data['start']


        print(page_number_inputted)
        print('page_number_inputted')
        if not search_data and not page_number_inputted:
            return JsonResponse({"message": "Please check input parameters."})
        page_number = page_number_inputted + 1      # actual page number
        last_record_num = page_number * num_records_per_page
        first_record_num = last_record_num - num_records_per_page
        print('first record number')
        print(first_record_num)
        print(last_record_num)
        # print(self.demo_func(4))
        if search_data == 'members':
            pass
        elif search_data == 'lab':
            pass
        else:
            # projects
            recordsTotal = Project.objects.all().count()
            object_list = list(Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt', 'body', 'authors', 'lab')).filter(search__icontains=search_data).distinct("pk").values()[first_record_num:last_record_num])            
            pass

    else:
        pass
    return JsonResponse({"draw":2, "recordsTotal": recordsTotal, "recordsFiltered": num_records_per_page, "data": object_list})
    
    # def demo_func(self, a):
    #     return a


@method_decorator(csrf_exempt)
def search_paginate_json_view(request):
    recordsTotal = None
    object_list = []

    if request.method == 'POST':
        print(request.body)
        # import pdb; pdb.set_trace()
        # print(d['start'])

        # search_data = request.POST.get('search[value]') if request.POST.get('search[value]') else None
        # page_number_inputted = int(request.POST.get('start')) if request.POST.get('start') else None
        
        
        json_data = json.loads(request.body)
        print(json_data)
        print(json_data['search[value]'])
        print(json_data['start'])
        search_data = json_data['search[value]']
        page_number_inputted = json_data['start']


        print(page_number_inputted)
        print('page_number_inputted')
        if not search_data and not page_number_inputted:
            return JsonResponse({"message": "Please check input parameters."})
        page_number = page_number_inputted + 1      # actual page number
        last_record_num = page_number * num_records_per_page
        first_record_num = last_record_num - num_records_per_page
        print('first record number')
        print(first_record_num)
        print(last_record_num)
        # print(self.demo_func(4))
        if search_data == 'members':
            pass
        elif search_data == 'lab':
            pass
        else:
            # projects
            recordsTotal = Project.objects.all().count()
            object_list = list(Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt', 'body', 'authors', 'lab')).filter(search__icontains=search_data).distinct("pk").values()[first_record_num:last_record_num])            
            pass

    else:
        pass
    return JsonResponse({"draw":2, "recordsTotal": recordsTotal, "recordsFiltered": num_records_per_page, "data": object_list})

@method_decorator(csrf_exempt)
def search_paginate_projects_view(request, search_data):
    qs_params = request.build_absolute_uri()    # querystring params
    splitted_params = qs_params.split('&')
    start = int(splitted_params[-5].split('=')[1])
    length = int(splitted_params[-4].split('=')[1])
    draw = int(splitted_params[0].split('=')[1])
    num_records_per_page = length    
    print('start')
    print(start)
    print(length)
    print(search_data)
    # start = 0
    recordsTotal = None
    object_list = []
    print(request.body)

    # search_data = request.POST.get('search[value]') if request.POST.get('search[value]') else None
    # page_number_inputted = int(request.POST.get('start')) if request.POST.get('start') else None
    search_data = search_data
    page_number_inputted = int(start)
    if not search_data and not page_number_inputted:
        return JsonResponse({"message": "Please check input parameters."})
    page_number = page_number_inputted + 1      # actual page number
    # last_record_num = page_number * num_records_per_page
    # first_record_num = last_record_num - num_records_per_page

    last_record_num = start + length
    first_record_num = start    

    # projects
    recordsTotal = Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt', 'body', 'authors', 'lab')).filter(search__icontains=search_data).distinct("pk").all().count()

    projects_data = Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt', 'body', 'authors', 'lab')).filter(search__icontains=search_data).distinct("pk").all()[first_record_num:last_record_num]

    object_list = list(Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt', 'body', 'authors', 'lab')).filter(search__icontains=search_data).distinct("pk").values()[first_record_num:last_record_num])

    parsed_uri = urlparse(request.build_absolute_uri())

    base_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
    owner_detail = None
    for ind, single in enumerate(projects_data):
        object_list[ind]['project_anchor_url'] = base_url+'/project/'+str(single.id)
        for single_member_comment in single.memberprojectcomment_set.all():
            if single_member_comment.is_owner == 1:
                object_list[ind]['owner_first_name'] = single_member_comment.member.first_name
                object_list[ind]['owner_last_name'] = single_member_comment.member.last_name
                object_list[ind]['owner_anchor_url'] = base_url+'/author/'+str(single_member_comment.member_id)
                object_list[ind]['owner_static_value'] = '(Owner)'

            if single_member_comment.member.professors.all().count() > 0:
                object_list[ind]['professor_first_name'] = single_member_comment.member.first_name
                object_list[ind]['professor_last_name'] = single_member_comment.member.last_name
                object_list[ind]['professor_anchor_url'] = base_url+'/author/'+str(single_member_comment.member_id)
                object_list[ind]['professor_static_value'] = '(Professor)'

        # single_member_comment = single.memberprojectcomment_set.filter(is_owner=1)
        # if single_member_comment:
        #     object_list[ind]['owner_first_name'] = single_member_comment[0].member.first_name
        #     object_list[ind]['owner_last_name'] = single_member_comment[0].member.last_name
        #     object_list[ind]['owner_anchor_url'] = base_url+'/author/'+str(single_member_comment[0].member_id)                

        # single.member.professors.all.count > 0









    # return JsonResponse({"draw":2, "recordsTotal": recordsTotal, "recordsFiltered": num_records_per_page, "data": object_list})
    return JsonResponse({"draw":draw, "recordsTotal": recordsTotal, "recordsFiltered": recordsTotal, "data": object_list})


@method_decorator(csrf_exempt)
def search_paginate_groups_view(request, search_data):
    qs_params = request.build_absolute_uri()    # querystring params
    splitted_params = qs_params.split('&')
    start = int(splitted_params[-5].split('=')[1])
    length = int(splitted_params[-4].split('=')[1])
    draw = int(splitted_params[0].split('=')[1])
    num_records_per_page = length

    recordsTotal = None
    object_list = []
    print(request.body)
    owner_details = []
    # search_data = request.POST.get('search[value]') if request.POST.get('search[value]') else None
    # page_number_inputted = int(request.POST.get('start')) if request.POST.get('start') else None
    search_data = search_data
    page_number_inputted = int(start)
    if not search_data and not page_number_inputted:
        return JsonResponse({"message": "Please check input parameters."})
    page_number = page_number_inputted + 1      # actual page number
    # last_record_num = page_number * num_records_per_page
    # first_record_num = last_record_num - num_records_per_page

    last_record_num = start + length
    first_record_num = start    

    # projects
    recordsTotal = Lab.objects.annotate(search=SearchVector('name', 'professors__first_name', 'professors__last_name', 'university__name', 'excerpt', 'owner', 'members__first_name', 'members__last_name')).filter(search__icontains=search_data).distinct("pk").count()
    labs_data = Lab.objects.annotate(search=SearchVector('name', 'professors__first_name', 'professors__last_name', 'university__name', 'excerpt', 'owner', 'members__first_name', 'members__last_name')).filter(search__icontains=search_data).distinct("pk")[first_record_num:last_record_num]

    object_list = list(Lab.objects.annotate(search=SearchVector('name', 'professors__first_name', 'professors__last_name', 'university__name', 'excerpt', 'owner', 'members__first_name', 'members__last_name')).filter(search__icontains=search_data).distinct("pk").values()[first_record_num:last_record_num])
    parsed_uri = urlparse(request.build_absolute_uri())
    base_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)
    
    # current_site = get_current_site(request)
    # print(current_site.domain)

    for ind, lab in enumerate(labs_data):
        object_list[ind]['owner_first_name'] = lab.owner.member.first_name
        object_list[ind]['owner_last_name'] = lab.owner.member.first_name
        object_list[ind]['owner_anchor_url'] = base_url+'/author/'+str(lab.owner.member.id)
        object_list[ind]['professor_anchor_url'] = base_url+'/author/'+str(list(lab.professors.values())[0]['id'])
        object_list[ind]['professor_first_name'] = list(lab.professors.values())[0]['first_name']
        object_list[ind]['professor_last_name'] = list(lab.professors.values())[0]['last_name']
        object_list[ind]['university_name'] = lab.university.name
        object_list[ind]['lab_anchor_url'] = base_url+'/lab/'+str(lab.id)        
    # object_list = list(labs_data.values()[first_record_num:last_record_num])

    
    print('object_list')
    print(object_list)
    return JsonResponse({"draw":draw, "recordsTotal": recordsTotal, "recordsFiltered": recordsTotal, "data": object_list})

@method_decorator(csrf_exempt)
def search_paginate_members_view(request, search_data):
    qs_params = request.build_absolute_uri()    # querystring params
    splitted_params = qs_params.split('&')
    start = int(splitted_params[-5].split('=')[1])
    length = int(splitted_params[-4].split('=')[1])
    draw = int(splitted_params[0].split('=')[1])
    num_records_per_page = length

    recordsTotal = None
    object_list = []
    print(request.body)

    # search_data = request.POST.get('search[value]') if request.POST.get('search[value]') else None
    # page_number_inputted = int(request.POST.get('start')) if request.POST.get('start') else None
    search_data = search_data
    page_number_inputted = int(start)
    if not search_data and not page_number_inputted:
        return JsonResponse({"message": "Please check input parameters."})
    page_number = page_number_inputted + 1      # actual page number
    # last_record_num = page_number * num_records_per_page
    # first_record_num = last_record_num - num_records_per_page
    last_record_num = start + length
    first_record_num = start

    # projects
    
    arr_members_ids = []
    arr_project_members_ids = []
    arr_lab_professors_ids = []
    arr_lab_members_ids = []
    print("[LOG] Query came")
    arr_members_ids = list(Member.objects.annotate(search=SearchVector('first_name', 'last_name', 'position', 'ideal_role', 'education', 'website', 'university__name')).filter(search__icontains=search_data).values_list('id', flat=True))

    print(arr_members_ids)

    # get filtered list from project model
    object_list2 = Project.objects.annotate(search=SearchVector('name', 'body', 'excerpt')).filter(search__icontains=search_data)
    
    
    for i in object_list2:
        # print(i.memberprojectcomment_set.values_list('id', flat=True))
        # arr_ids.append(list(i.memberprojectcomment_set.values_list('id', flat=True)))
        arr_project_members_ids += list(i.memberprojectcomment_set.values_list('member_id', flat=True))

    # get filtered list from lab model
    object_list3 = Lab.objects.annotate(search=SearchVector('name', 'excerpt', 'university__name', 'major_contributions', 'sources_of_funding')).filter(search__icontains=search_data)


    for i in object_list3:

        # assign lab professors ids into array
        for single_professor in i.professors.values():
            # arr_lab_professors_ids += single_professor.id
            arr_lab_professors_ids.append(single_professor['id'])

        # assign lab members ids into array
        for single_member in i.members.values():
            arr_lab_members_ids.append(single_member['id'])


    all_members_ids = list(set(arr_members_ids + arr_project_members_ids + arr_lab_professors_ids + arr_lab_members_ids))
    print('all_members_ids')
    print(all_members_ids)
    print('length')
    print(Member.objects.filter(id__in=all_members_ids).distinct('id').all())
    members_data = Member.objects.filter(id__in=all_members_ids).distinct('id').all()[first_record_num:last_record_num]

    recordsTotal = Member.objects.filter(id__in=all_members_ids).distinct('id').all().count()
    object_list = list(Member.objects.filter(id__in=all_members_ids).distinct('id').values()[first_record_num:last_record_num])
    print(type(object_list))
    print(object_list)
    parsed_uri = urlparse(request.build_absolute_uri())
    base_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)    
    for ind, single in enumerate(members_data):
        object_list[ind]['member_anchor_url'] = base_url+'/author/'+str(single.id)
        object_list[ind]['member_img_url'] = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(single.image)
        object_list[ind]['university_name'] = single.university.name


    return JsonResponse({"draw":draw, "recordsTotal": recordsTotal, "recordsFiltered": recordsTotal, "data": object_list})


class RemoveSessionsView(View):
    def get(self, request):
        # if sessions exist, remove them
        if 'authors_arr' in self.request.session:
            del self.request.session['authors_arr']
        if 'comments_arr' in self.request.session:
            del self.request.session['comments_arr']  
        if 'is_preview_on_edit' in self.request.session:
            del self.request.session['is_preview_on_edit']
        if 'previewed_project_id' in self.request.session:
            del self.request.session['previewed_project_id']

        if 'professors_arr' in self.request.session:
            del self.request.session['professors_arr']
        if 'students_arr' in self.request.session:
            del self.request.session['students_arr']
        if 'lab_research_list' in self.request.session:
            del self.request.session['lab_research_list']
        if 'lab_publication_list' in self.request.session:
            del self.request.session['lab_publication_list']                        


        # return HttpResponse('sessions have been removed')    
        return JsonResponse({"message":"Sessions removed successfully."})  
 
class UploadView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    form_class = AuthorForm
    success_url = "/"
    template_name="projects/upload.html"
    # todo
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['years'] = ['',2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019]
        context['degrees'] = ['','MCA', 'BCA', 'BBA', 'MBA']
        login_member_info = None
        if self.request.user.is_authenticated:
            is_user_member = Member.objects.filter(user_id=self.request.user.customuser.id).values()
            if is_user_member:
                login_member = self.request.user.customuser.member
                if str(login_member.image):
                    img_url = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(login_member.image)
                else:
                    img_url = None            

                if login_member.university_id:
                    uni_name = login_member.university.name
                login_member_info = json.dumps({'id':login_member.id, 'user_id':login_member.user_id, 'first_name':login_member.first_name, 'last_name':login_member.last_name, 'university':uni_name, 'position': login_member.position, 'ideal_role': login_member.ideal_role, 'image': img_url })
            
            
            members = Member.objects.all().exclude(first_name__isnull=True, last_name__isnull=True, image__isnull=True, position__isnull=True, ideal_role__isnull=True, website__isnull=True).exclude(first_name__exact='', last_name__exact='')
        else:
            current_user = None
            members = Member.objects.none()
        context['members']=members
        is_user_member = list(Member.objects.filter(user_id=self.request.user.customuser.id).all().values())
        if is_user_member:
            context['login_owner_id'] = self.request.user.customuser.member.id
            context['login_owner_first_name'] = self.request.user.customuser.member.first_name
            context['login_owner_last_name'] = self.request.user.customuser.member.last_name
        else:
            context['login_owner_id'] = None
            context['login_owner_first_name'] = None
            context['login_owner_last_name'] = None

        context['login_member_info'] = login_member_info

        return context


    def post(self, request, *args, **kwargs):
        if request.POST.get('post_type') == 'author_upload':
            first_name = request.POST.get('first_name') if request.POST.get('first_name') else ''
            last_name = request.POST.get('last_name') if request.POST.get('last_name') else ''
            position = request.POST.get('position') if request.POST.get('position') else ''
            institute = request.POST.get('institute') if request.POST.get('institute') else ''
            website = request.POST.get('website') if request.POST.get('website') else ''
            email = request.POST.get('email') if request.POST.get('email') else ''
            ideal_role = request.POST.get('ideal_role') if request.POST.get('ideal_role') else ''
            education = json.loads(request.POST.get('education'))
            companies = json.loads(request.POST.get('company'))
            member_insert = Member(first_name= first_name, last_name = last_name, position = position, university_id = institute, website= website, ideal_role=ideal_role, email=email)
            member_insert.save()
            latest_record = Member.objects.all().order_by('-id').values()[0]
            # save education records in MemberQualification model
            for single_edu in education:
                MemberQualification(member_id= latest_record['id'], university_id = single_edu['name'],degree=single_edu['degree'], year=single_edu['year']).save()
            # save work experience in MemberWorkExperience model
            for company in companies:
                MemberWorkExperience(member_id= latest_record['id'], company = company['name'],position=company['position'], yearfrom=company['from'], yearto=company['to']).save()

            university_name = University.objects.get(id=latest_record['university_id']).name

            file = request.FILES['avatar']
            fs = FileSystemStorage()
            fs.save(file.name,file) # fs.save(<file path with name>, file)

            local_directory = 'media/'
            for root, dirs, files in os.walk(local_directory):
                for filename in files:
                    local_path = os.path.join(root, filename)
                    relative_path = os.path.relpath(local_path, local_directory)
                    s3_path = os.path.join('uploads/profile-photos',relative_path)
                    s3_handler = S3Handler()
                    s3_handler.upload_file(file_path=local_path, file_name=filename, s3_path= s3_path)
                    os.remove('media/%s'%filename)
                    # update existing record with uploaded file name
                    update_id = Member.objects.get(id=latest_record['id'])
                    update_id.image= 'uploads/profile-photos/'+filename
                    update_id.save()


            result = { 'member_id': latest_record['id'], 'first_name': latest_record['first_name'], 'last_name': latest_record['last_name'], 'university_name': university_name}

            return JsonResponse(result)
        # demo upload
        elif request.POST.get('post_type') == 'demo_upload':
            print('coming in demo upload')
            image = request.FILES['image'] if 'image' in request.FILES else None
            image_upload = self.upload_file_locally(image, 'banner', 1) if image else None
            return JsonResponse(image_upload, safe=False)

        # publish button clicked. save info into db.
        elif request.POST.get('post_type') == 'project_upload':
            image = request.FILES['image'] if 'image' in request.FILES else None
            name = request.POST.get('name') if request.POST.get('name') else None
            excerpt = request.POST.get('excerpt') if request.POST.get('excerpt') else None
            body = request.POST.get('body') if request.POST.get('body') else None
            image1 = request.FILES['image1'] if 'image1' in request.FILES else None
            image2 = request.FILES['image2'] if 'image2' in request.FILES else None
            image3 = request.FILES['image3'] if 'image3' in request.FILES else None
            authors_arr = request.POST.get('authors_arr')
            temp_authors_arr = json.loads(authors_arr)
            project_id = self.save_project(image, name, excerpt, body, image1, image2, image3, temp_authors_arr)     # save project details
            self.save_comments(project_id)    # save members comments

            # remove sessions after submitting of data.
            del self.request.session['comments_arr']
            del self.request.session['authors_arr']
            return JsonResponse('Project Created Successfully.', safe=False)
        elif request.POST.get('post_type') == 'author_project_comment':
            status = 0
            comments = []
            comment = request.POST.get('comment')
            project_member = int(request.POST.get('project_member'))

            # if comments_arr session already exists, then check for already existing project member, if it already exists then update it otherwise push into an array.
            if 'comments_arr' in self.request.session:
                sess_comments = json.loads(self.request.session['comments_arr'])
                if len(sess_comments) > 1:
                    for sess_comment in sess_comments:
                        # if project member already exists, then update it otherwise append into ses_comments array.
                        if sess_comment['project_member'] == project_member:
                            status = 1
                            sess_comment['comment'] = comment
                            break

                if status == 0:
                    sess_comments.append({'comment':comment, 'project_member':project_member})
                self.request.session['comments_arr'] = json.dumps(sess_comments)

            else:
                comments.append({'comment':comment, 'project_member':project_member})
                self.request.session['comments_arr'] = json.dumps(comments)

            return JsonResponse(comments, safe=False)
        
        else:
            # save authors and their comments temporarily in session by clicking on preview button
            if request.POST.get('post_type') == 'author_save_temp':
                authors_no_contribution = None
                authors_no_contribution = self.save_new_upload_page_authors(request.POST.get('authors_arr'))

                return JsonResponse({"authors_exist":authors_no_contribution, "comments_arr":self.request.session['comments_arr'] if 'comments_arr' in self.request.session else None})

    
    # save new upload page authors in sessions and check their contributions
    def save_new_upload_page_authors(self, new_authors_arr):
        authors_no_contribution = []
        comments_arr = []
        if 'authors_arr' in self.request.session:
            del self.request.session['authors_arr'] # delete existing authors_arr session if already existing.
        authors_arr = json.loads(new_authors_arr)
        # upload page
        if 'comments_arr' in self.request.session:
            comments_arr = json.loads(self.request.session['comments_arr'])
            temp_comments_arr = comments_arr
            # if other members comments exist, then we will be putting them in a new object authors_arr variable otherwise we need to generate error in response to again try.comments
            for author in authors_arr:
                status = 0
                for ind, comment in enumerate(temp_comments_arr):
                    if int(comment['project_member']) == int(author['id']):
                        if comment['comment']:
                            status = 1
                            author['contribution'] = comment['comment']
                            temp_comments_arr.pop(ind)
                            break
                        
                if status == 0:    
                    authors_no_contribution.append({'first_name':author['first_name'], 'last_name':author['last_name']})

        else:
            # when no comments_arr session exists.
            for author in authors_arr:
                authors_no_contribution.append({'first_name':author['first_name'], 'last_name':author['last_name']})
        self.request.session['authors_arr'] = json.dumps(authors_arr)
        return authors_no_contribution

    def save_project(self, image, name, excerpt, body, image1, image2, image3, authors_arr):
        image = self.upload_file_locally(image, 'banner', 1) if image else None
        image1 = self.upload_file_locally(image1, 'upm-img1', 0) if image1 else None
        image2 = self.upload_file_locally(image2, 'upm-img2', 0) if image2 else None
        image3 = self.upload_file_locally(image3, 'upm-img3', 0) if image3 else None
        self.upload_file_to_s3() # upload all files to amazon s3
        save_project = Project(image=image, name=name, excerpt=excerpt, body=body, image1=image1, image2=image2, image3=image3)
        save_project.save()
        # getting authors from request session
        login_user_status = 0
        for author in authors_arr:
            if self.request.user.customuser.member.id == int(author['id']):
                login_user_status = 1
                
            get_author = Member.objects.get(id=int(author['id']))
            save_project.authors.add(get_author)

        if login_user_status == 0:
            get_author = Member.objects.get(id=self.request.user.customuser.member.id)
            save_project.authors.add(get_author)            

        return save_project.id

    def crop_image_and_save(self, filename, img_type):
        x = float(self.request.POST.get(f'x_{img_type}'))
        y = float(self.request.POST.get(f'y_{img_type}'))
        w = float(self.request.POST.get(f'w_{img_type}'))
        h = float(self.request.POST.get(f'h_{img_type}'))
        img = Image.open(f'media/{filename}')
        cropped_img = img.crop((x, y, w+x, h+y))
        # resized_img = cropped_img.resize((200, 200), Image.ANTIALIAS)
        cropped_img.save(f'media/{filename}')
        return filename

    def upload_file_locally(self, file, img_type, is_file_cropped):
        file_name = file.name.split(".")[0]+'_'+str(random.randint(9999,9999999))
        full_file_name = file_name+os.path.splitext(file.name)[1]
        fs = FileSystemStorage()
        saved_filename = fs.save(full_file_name,file) # fs.save(<file path with name>, file)
        # print('saved_filename')
        # print(saved_filename)
        # pdb.set_trace()
        # cropped_saved_filename = self.crop_image_and_save(saved_filename, img_type)
        if is_file_cropped == 0:
            cropped_saved_filename = self.crop_image_and_save(saved_filename, img_type)
        else:
            cropped_saved_filename = saved_filename
        return 'uploads/project-photos/'+ cropped_saved_filename

    def upload_file_to_s3(self):
        local_directory = 'media/'
        for root, dirs, files in os.walk(local_directory):
            for filename in files:
                local_path = os.path.join(root, filename)
                relative_path = os.path.relpath(local_path, local_directory)
                s3_path = os.path.join('uploads/project-photos',relative_path)
                s3_handler = S3Handler()
                s3_handler.upload_file(file_path=local_path, file_name=filename, s3_path= s3_path)
                os.remove('media/%s'%filename)

    def authors_save_temp(self, authors_arr):
        self.request.session['authors_arr'] = authors_arr
        return 'hello'

    def save_comments(self, project_id):
        authors_arr = []
        if 'authors_arr' in self.request.session:
            authors_arr = json.loads(self.request.session['authors_arr'])
            if len(authors_arr) > 0:
                
                # save login user contribution
                for single_comment in json.loads(self.request.session['comments_arr']):
                    if single_comment['project_member'] == self.request.user.customuser.member.id:
                        MemberProjectComment(member_id=int(single_comment['project_member']), project_id=int(project_id), comment = single_comment['comment'], is_owner = 1).save()
                    else:
                        MemberProjectComment(member_id=single_comment['project_member'], project_id=project_id, comment = single_comment['comment']).save()

            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

# delete comments reside in session to the corresponding comment and member id by clicking on cross sign
class DeleteCommentAjaxView(UserPassesTestMixin, View):
    def get(self, request):
        member_id = int(request.GET.get('member_id'))
        project_id = int(request.GET.get('project_id')) if request.GET.get('project_id') else None
        message = None
        comments_arr = []
        login_user_status = 0
        if not project_id:
            if member_id == self.request.user.customuser.member.id:
                login_user_status = 1
                message = 'Member is the owner (Project yet not created), It cant be removed.'
            elif 'comments_arr' in self.request.session:
                comments_arr = json.loads(self.request.session['comments_arr'])
                for index, single_comment in enumerate(comments_arr):
                    if member_id == self.request.user.customuser.member.id and member_id == single_comment['project_member']:
                        login_user_status = 1
                        message = 'Member is the owner (Project yet not created), It cant be removed.'
                        break
                    elif member_id == single_comment['project_member']:
                        del comments_arr[index]
                        message = 'Member Deleted Successfully.'
                        del self.request.session['comments_arr']    # delete session array
                        self.request.session['comments_arr'] = json.dumps(comments_arr) # assign again after deletion of comments_arr session.
                        break
                    else:
                        message = 'Member does not exist.'

                
            else:
                message = 'Session does not exist.'            
        else:
            # query to check whether requested user for deletion is current login user or not, if he is current login user, then we dont need to delete him.
            is_user_login = MemberProjectComment.objects.filter(member_id=member_id, project_id=project_id, is_owner = 1).values()
            # if author is an owner, he can't be removed by other author
            if is_user_login:
                login_user_status = 1
                message = 'Member is the owner, It cant be removed.'
            # if author is login to login, he can't remove himself. 
            elif member_id == self.request.user.customuser.member.id:
                login_user_status = 1
                message = "Login member can't be removed by itself."
            elif 'comments_arr' in self.request.session:
                comments_arr = json.loads(self.request.session['comments_arr'])
                for index, single_comment in enumerate(comments_arr):
                    if member_id == single_comment['project_member']:
                        del comments_arr[index]
                        del self.request.session['comments_arr']    # delete session array
                        self.request.session['comments_arr'] = json.dumps(comments_arr) # assign again after deletion of comments_arr session.
                        message = 'Member Deleted Successfully.'                        
                        break
                    else:
                        message = 'Member does not exist.'

            else:
                message = 'Session does not exist.'

        return JsonResponse({"is_user_login": login_user_status, "message": message}, safe=False)
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True





class LabPreviewView(UserPassesTestMixin, TemplateView):
    template_name = 'projects/lab_preview.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'authors_arr' in self.request.session:
            context['students_arr'] = json.loads(self.request.session['students_arr'])
        else:
            context['students_arr'] = []


        if 'professors_arr' in self.request.session:
            professors_arr = []
            professors = json.loads(self.request.session['professors_arr'])
            all_members = Member.objects.all()
            for single_prof in professors:
                detail = [single_member for single_member in all_members if single_member.id == int(single_prof['id'])][0]
                professors_arr.append({'id':detail.id, 'image': detail.image_url, 'author_name':detail.first_name+' '+detail.last_name, 'position':detail.position})

            context['professors_arr'] = professors_arr
        else:
            context['professors_arr'] = []
        
        if 'lab_research_list' in self.request.session:
            lab_research_list = json.loads(self.request.session['lab_research_list'])
            research_list = []
            c = 0
            for research in lab_research_list:
                name = research.get('name')
                excerpt = research.get('excerpt')
                research_list.append({'name': name, 'excerpt': excerpt })
                c += 1
            context['lab_research_list'] = research_list
        else:
            context['lab_research_list'] = []
        if 'lab_publication_list' in self.request.session:
            lab_publication_list = json.loads(self.request.session['lab_publication_list'])
            publication_list = []
            for pub in lab_publication_list:
                citation = pub.get('citation')
                publication_list.append({'citation': citation})
            context['lab_publication_list'] = publication_list
        else:
            context['lab_publication_list'] = []

        return context
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True


# add UserpassesMixin
class LabEditView(LoginRequiredMixin, UserPassesTestMixin, TemplateView):
    def get(self, request, pk, **kwargs):
        lab = get_object_or_404(Lab, pk=pk)
        uni_id = lab.university.id
        universities = University.objects.exclude(id=uni_id)
        professors = None


        if self.request.user.is_authenticated:
            # professors = 
            members = Member.objects.all().exclude(first_name__isnull=True, last_name__isnull=True, image__isnull=True, position__isnull=True, ideal_role__isnull=True, website__isnull=True).exclude(first_name__exact='', last_name__exact='').filter(user__type='SU')
        else:
            current_user = None
            members = Member.objects.none()
        
        print('present user name')
        # is login lab admin exists as a member
        is_admin_a_member = Member.objects.filter(user_id=self.request.user.customuser.id)
        
        # filter members based on their user type
        filtered_members = lab.members.filter(user__type='SU')

        # if is_admin_a_member:
        #     lab.members.set(chain(is_admin_a_member, filtered_members ))
        # else:
        #     lab.members.set(filtered_members)
        lab.members.set(filtered_members)

        # CustomUser.objects.get()

        data = {'lab': lab, 'form': ProjectForm(), 'p_form':ProfessorForm(),'members': members, 'universities': universities, "is_admin_a_member":is_admin_a_member}
        # if sessions exist, remove them
        if 'authors_arr' in self.request.session:
            del self.request.session['authors_arr']
        if 'students_arr' in self.request.session:
            del self.request.session['students_arr']
        if 'professors_arr' in self.request.session:
            del self.request.session['professors_arr']
        if 'comments_arr' in self.request.session:
            del self.request.session['comments_arr']
        if 'lab_research_list' in self.request.session:
            del self.request.session['lab_research_list']
        if 'lab_publication_list' in self.request.session:
            del self.request.session['lab_publication_list']
        return render(request, 'projects/lab_upload.html', data)

    def post(self, request, pk, **kwargs):
        lab = get_object_or_404(Lab, pk=pk)
        if lab.owner.id == request.user.customuser.id:
            if request.POST.get('post_type') == 'lab_upload':
                name = request.POST.get('name') if request.POST.get('name') else ''
                member_arr = request.POST.get('students_arr')
                professors_arr = request.POST.get('professors_arr')
                university_id = request.POST.get('university') if request.POST.get('university') else ''
                excerpt = request.POST.get('excerpt') if request.POST.get('excerpt') else ''
                major_contri = request.POST.get('major_contri') if request.POST.get('major_contri') else ''
                src_fund = request.POST.get('source_of_funding') if request.POST.get('source_of_funding') else ''
                research_list = json.loads(request.POST.get('lab_research_list'))
                publications = json.loads(request.POST.get('lab_publication_list'))
                
                
                university = get_object_or_404(University, id=university_id)

                Lab.objects.filter(pk=pk).update(name=name, excerpt=excerpt, university=university, major_contributions=major_contri,sources_of_funding=src_fund)
                lab.members.clear()
                for member in json.loads(member_arr):
                    get_member = Member.objects.get(id=member['id'])
                    lab.members.add(get_member)

                # update professors
                lab.professors.clear()
                for professor in json.loads(professors_arr):
                    get_professor = Member.objects.get(id=int(professor['id']))
                    lab.professors.add(get_professor)

                # deleting 
                for r in lab.labresearchtopic_set.all():
                    for research in research_list:
                        if r.id == research.get('id'):
                            break
                    else:
                        r.delete()
                # updating
                for r in lab.labresearchtopic_set.all():
                    for research in research_list:
                        if r.id == research.get('id'):
                            r.name = research.get('name')
                            r.excerpt = research.get('excerpt')
                            if research.get('image') != 'same':
                                image = self.upload_file_locally(request.FILES.get(research.get('image')), 'update_image_' + str(r.id) ) if request.FILES.get(research.get('image')) else None
                                r.image = image
                            else:
                                pass
                            r.save()
                            research_list.remove(research)

                # adding new            
                c = 0
                for research in research_list:
                    name = research.get('name')
                    excerpt = research.get('excerpt')
                    image = self.upload_file_locally(request.FILES.get('image_'+str(c)), 'image_'+str(c)) if request.FILES.get('image_'+ str(c)) else None
                    # self.upload_file_locally(image)
                    lab_research = LabResearchTopic.objects.create(name=name, image=image, lab=lab, excerpt=excerpt)
                    c += 1

                self.upload_file_to_s3()
                
                for pub in lab.labpublication_set.all():
                    pub.delete()

                for pub in publications:
                    citation = pub.get('citation')
                    lab_pub = LabPublication.objects.create(citation=citation, lab=lab)

                
                
                return JsonResponse('Lab Created Successfully.', safe=False)
            elif request.POST.get("post_type") == "lab_upload_preview":
                lab_research_list = []
                lab_publication_list = []

                research_arr = json.loads(request.POST.get('lab_research_list'))
                publication_arr = json.loads(request.POST.get('lab_publication_list'))
                students_arr = json.loads(request.POST.get('students_arr'))
                request.session['students_arr'] = json.dumps(students_arr)


                professors_arr = json.loads(request.POST.get('professors_arr'))
                request.session['professors_arr'] = json.dumps(professors_arr)
                if 'lab_publication_list' in request.session:
                    print('exist')
                else:
                    for pub in publication_arr:
                        citation = pub.get('citation')
                        lab_publication_list.append({'citation': citation})
                    request.session['lab_publication_list'] = json.dumps(lab_publication_list)
                
                # for lab research topics
                if 'lab_research_list' in request.session:
                    print('research list exist')
                else:
                    for r in lab.labresearchtopic_set.all():
                        for l in research_arr:
                            if r.id == l.get('id'):
                                _id = l.get('id')
                                name = l.get('name')
                                excerpt = l.get('excerpt')
                                image = l.get('image')
                                if l.get('image') == 'same':
                                    image_url = l.get('image_url')
                                    lab_research_list.append({ 'id': _id,'name': name, 'excerpt': excerpt, 'image': image, 'image_url': image_url})
                                else:
                                    lab_research_list.append({ 'id': _id,'name': name, 'excerpt': excerpt, 'image': image})
                                research_arr.remove(l)
                        else:
                            pass
                    for l in research_arr:
                        name = l.get('name')
                        excerpt = l.get('excerpt')
                        image = l.get('image')
                        lab_research_list.append({'name': name, 'excerpt': excerpt, 'image': image})
                    request.session['lab_research_list'] = mark_safe(json.dumps(lab_research_list))
                return JsonResponse('setting up research and publication', safe=False)
            elif request.POST.get("post_type") == "author_save_temp":
                print("author_save_temp")
                pass
            else:
                return JsonResponse('nothing', safe=False)
        else:
            return redirect('home')
      
        return JsonResponse('Lab does not updated!.', safe=False)

    def upload_file_locally(self, file, img_type):
        file_name = file.name.split(".")[0]+'_'+str(random.randint(9999,9999999))
        full_file_name = file_name+os.path.splitext(file.name)[1]
        fs = FileSystemStorage()
        saved_filename = fs.save(full_file_name,file) # fs.save(<file path with name>, file)
        file_name = self.crop_image_and_save(saved_filename, img_type)
        return 'uploads/lab-research-photos/'+file_name

    def upload_file_to_s3(self):
        local_directory = 'media/'
        for root, dirs, files in os.walk(local_directory):
            for filename in files:
                local_path = os.path.join(root, filename)
                relative_path = os.path.relpath(local_path, local_directory)
                s3_path = os.path.join('uploads/lab-research-photos',relative_path)
                s3_handler = S3Handler()
                s3_handler.upload_file(file_path=local_path, file_name=filename, s3_path= s3_path)
                os.remove('media/%s'%filename)

    def crop_image_and_save(self, filename, img_type):
        x = float(self.request.POST.get(f'x_{img_type}'))
        y = float(self.request.POST.get(f'y_{img_type}'))
        w = float(self.request.POST.get(f'w_{img_type}'))
        h = float(self.request.POST.get(f'h_{img_type}'))
        img = Image.open(f'media/{filename}')
        cropped_img = img.crop((x, y, w+x, h+y))
        # resized_img = cropped_img.resize((200, 200), Image.ANTIALIAS)
        cropped_img.save(f'media/{filename}')
        return filename
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True


    


class PreviewView(UserPassesTestMixin, TemplateView):

    template_name = 'projects/preview.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'authors_arr' in self.request.session:
            context['authors_arr'] = json.loads(self.request.session['authors_arr'])

        else:
            context['authors_arr'] = []

        return context
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

# class AuthorAutocompleteView(autocomplete.Select2QuerySetView):
#     def get_queryset(self):
#         # Don't forget to filter out results depending on the visitor !
#         if not self.request.user.is_authenticated:
#             return Member.objects.none()

#         qs = Member.objects.filter(Q(user__type='SU')| Q(user__type='PF')).exclude(first_name__isnull=True, last_name__isnull=True, image__isnull=True, position__isnull=True, ideal_role__isnull=True, website__isnull=True).exclude(first_name__exact='', last_name__exact='').exclude(id=self.request.user.customuser.member.id)


#         if self.q:
#             qs = qs.filter(Q(first_name__istartswith=self.q) | Q(last_name__istartswith=self.q) )
#         # pdb.set_trace()
#         return qs

    def get_result_label(self, Member):
        # jsondump = json.dumps(Member)
        img_url = None
        uni_name = None
        if Member is not None:
            if str(Member.image):
                img_url = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(Member.image)
            else:
                img_url = None

            if Member.university_id:
                uni_name = Member.university.name
            member_detail = json.dumps({'id':Member.id, 'user_id':Member.user_id, 'first_name':Member.first_name, 'last_name':Member.last_name, 'university':uni_name, 'position': Member.position, 'ideal_role': Member.ideal_role, 'image': img_url })
            
        # return format_html("%s %s, %s"(Member.first_name, Member.last_name, uni_name))
        return format_html('<input type="hidden" class="memberid" id="{}" data="{}">{} {}, {}', Member.id, member_detail, Member.first_name, Member.last_name, uni_name)

    # def get_selected_result_label(self, Member):
    #     return Member.first_name


class AuthorAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Member.objects.none()

        qs = Member.objects.filter(Q(user__type='SU')| Q(user__type='PF')).exclude(first_name__isnull=True, last_name__isnull=True, image__isnull=True, position__isnull=True, ideal_role__isnull=True, website__isnull=True).exclude(first_name__exact='', last_name__exact='').exclude(id=self.request.user.customuser.member.id)


        if self.q:
            qs = qs.filter(Q(first_name__istartswith=self.q) | Q(last_name__istartswith=self.q) )

        return qs

    def get_result_label(self, Member):
        # jsondump = json.dumps(Member)
        img_url = None
        uni_name = None
        if Member is not None:
            if str(Member.image):
                img_url = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(Member.image)
            else:
                img_url = None

            if Member.university_id:
                uni_name = Member.university.name

            member_detail = json.dumps({'id':Member.id, 'user_id':Member.user_id, 'first_name':Member.first_name, 'last_name':Member.last_name, 'university':uni_name, 'position': Member.position, 'ideal_role': Member.ideal_role, 'image': img_url })

        # return format_html(uni_name)
        return format_html('<input type="hidden" class="memberid" id="{}" data="{}">{} {}, {}', Member.id, member_detail, Member.first_name, Member.last_name, uni_name)
        # return format_html("%s %s, %s"%(Member.first_name, Member.last_name, uni_name))

class UniversityAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return University.objects.none()        
        qs = University.objects.all()
        if self.q:
            qs = qs.filter(name__istartswith=self.q)
        return qs

    def get_result_label(self, University):
        # jsondump = json.dumps(Member)
        if University is not None:
            university_detail = json.dumps({'id':University.id, 'name': University.name })
        return format_html('<input type="hidden" class="universityid" id="{}" data="{}">{}', University.id, university_detail, University.name)



        # where is its form class and url path? html file---profile.html?  yes

class MemberAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Member.objects.none()

        qs = Member.objects.filter(user__type='SU').exclude(first_name__isnull=True, last_name__isnull=True, image__isnull=True, position__isnull=True, ideal_role__isnull=True, website__isnull=True).exclude(first_name__exact='', last_name__exact='')


        if self.q:
            qs = qs.filter(Q(first_name__istartswith=self.q) | Q(last_name__istartswith=self.q) )

        return qs

    def get_result_label(self, Member):
        img_url = None
        uni_name = None
        if Member is not None:
            if str(Member.image):
                img_url = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(Member.image)
            else:
                img_url = None

            if Member.university_id:
                uni_name = Member.university.name

            member_detail = json.dumps({'id':Member.id, 'user_id':Member.user_id, 'first_name':Member.first_name, 'last_name':Member.last_name, 'university':uni_name, 'position': Member.position, 'ideal_role': Member.ideal_role, 'image': img_url })

        return format_html('<input type="hidden" class="memberid" id="{}" data="{}">{} {}, {}', Member.id, member_detail, Member.first_name, Member.last_name, uni_name)


class ProfessorAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated:
            return Member.objects.none()

        qs = Member.objects.filter(user__type='PF').exclude(first_name__isnull=True, last_name__isnull=True, image__isnull=True, position__isnull=True, ideal_role__isnull=True, website__isnull=True).exclude(first_name__exact='', last_name__exact='')

        if self.q:
            qs = qs.filter(Q(first_name__istartswith=self.q) | Q(last_name__istartswith=self.q) )
        return qs

    def get_result_label(self, Member):
        img_url = None
        uni_name = None
        if Member is not None:
            if str(Member.image):
                img_url = 'https://'+settings.AWS_S3_CUSTOM_DOMAIN+'/'+str(Member.image)
            else:
                img_url = None

            if Member.university_id:
                uni_name = Member.university.name

            member_detail = json.dumps({'id':Member.id, 'user_id':Member.user_id, 'first_name':Member.first_name, 'last_name':Member.last_name, 'university':uni_name, 'position': Member.position, 'ideal_role': Member.ideal_role, 'image': img_url })

        return format_html('<input type="hidden" class="memberid" id="{}" data="{}">{} {}, {}', Member.id, member_detail, Member.first_name, Member.last_name, uni_name)


class LikesView(UserPassesTestMixin, View):
    def get(self, request):
        userLoggedIn = None
        user_like = None
        total_likes = None
        project_id = request.GET.get('project_id')
        if request.user.is_authenticated:
            user = get_object_or_404(CustomUser,id=request.user.customuser.id)
            user_id = user.id
            user_like_query = list(Userlikes.objects.all().filter(user=user, project_id=project_id).values())
            if not user_like_query:
                userlike_model = Userlikes(user_id=user_id, project_id=project_id, like=1)
                userlike_model.save()

                project_model = Project.objects.get(id=project_id)
                project_model.likes += 1
                project_model.save()
                user_like = 1
                total_likes = project_model.likes
            else:
                if user_like_query[0]['like'] == 1:
                    user_liked_query = Userlikes.objects.get(user_id=user_id, project_id=project_id, like=1)
                    user_liked_query.like=0
                    user_liked_query.save()

                    project_model = Project.objects.get(id=project_id)
                    project_model.likes -= 1
                    project_model.save()
                    user_like = 0
                    total_likes = project_model.likes
                else:
                    if user_like_query[0]['like'] == 0:
                        user_liked_query = Userlikes.objects.get(user_id=user_id, project_id=project_id, like=0)
                        user_liked_query.like=1
                        user_liked_query.save()

                        project_model = Project.objects.get(id=project_id)
                        project_model.likes += 1
                        project_model.save()
                        user_like = 1
                        total_likes = project_model.likes
            
        result = {"user_like": user_like, "total_likes": total_likes}

        return JsonResponse(result)
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

class SaveAuthorView(UserPassesTestMixin, View):
    def get(self, request):
        firstname = request.GET.get('first_name', None)
        return JsonResponse(firstname, safe=False)
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True

class GetUniversityListView(UserPassesTestMixin, View):
    def get(self,request):
        return JsonResponse(list(University.objects.all().values()), safe=False)
            
    def get_test_func(self):
            return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True



@login_required
@staff_member_required
def new_lab_users_list(request):
    new_lab_users = CustomUser.objects.filter(type='LA')
    form = LabAdminUserForm()
    context = {
        'users': new_lab_users,
        'form': form
    }
    return render(request, 'projects/new_lab_admin_approval.html', context)

@login_required
@staff_member_required
def new_lab_user_action(request, action, user_id=None):
    subject = 'Request to join ForeFront as Lab Admin User'
    current_site = get_current_site(request)
    if action == 'A'and request.method == 'POST':
        user_id = int(request.POST.get('user_id'))
        cuser = get_object_or_404(CustomUser, id=user_id)
        cuser.status = 1 #approved
        cuser.save()
        email = cuser.user.email
        
        unvrsty_id = int(request.POST.get('university')[0])
        un = get_object_or_404(University, id=unvrsty_id)
        
        lab_name = request.POST.get('lab_name')

        new_lab = Lab.objects.create(owner=cuser, name=lab_name, university=un)
        parsed_uri = urlparse(request.build_absolute_uri())
        base_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)        
        message = render_to_string('projects/request_to_join.html',{
            'user': cuser,
            'status': 1,
            'domain': base_url
        })
        Utilities().email_user(subject=subject, message=message, to_email=email)
        # mail_user(subject, message, email)
        
        return redirect(reverse('new_lab_user'))
    elif action == 'D' and user_id >= 0:
        cuser = get_object_or_404(CustomUser, id=user_id)
        cuser.status = 0
        cuser.save()

        email = cuser.user.email
        message = render_to_string('projects/request_to_join.html',{
            'user': cuser,
            'status': 0,
            'domain': current_site.domain
        })
        # mail_user(subject, message, email)
        Utilities().email_user(subject=subject, message=message, to_email=email)
        return redirect(reverse('new_lab_user'))
    elif user_id >= 0:
        cuser = get_object_or_404(CustomUser, id=user_id)
        email = cuser.user.email
        cuser.delete()
        return redirect(reverse('new_lab_user'))


class UserInvitationView(LoginRequiredMixin, UserPassesTestMixin, View):
    def get(self, request):
        print('herere')
        form = UserInvitationForm(request)
        return render(request, 'projects/user-invitation.html',{'form':form})

    def post(self, request):
        project_id = request.POST.get('projects') if request.POST.get('projects') else None
        email = request.POST.get('email') if request.POST.get('email') else None

        project_name=Project.objects.filter(id=project_id)[0].name
        print(project_name)

        username = "%s %s"%(self.request.user.customuser.member.first_name,self.request.user.customuser.member.last_name)

        parsed_uri = urlparse(request.build_absolute_uri())
        site_url = '{uri.scheme}://{uri.netloc}'.format(uri=parsed_uri)

        Utilities().email_user(subject='Forefront', message='Hello <br/><br/>'+username+' is inviting you to be mentioned as a co-author on the '+project_name+' project.<br/>Please <a href="'+site_url+'">click here</a> to go to site.<br/><br/>Thanks<br/>Forefront Team', to_email=email)
        # mail_user('Forefront', 'Hello <br/><br/>'+username+' is inviting you to be mentioned as a co-author on the '+project_name+' project.<br/>Please <a href="'+site_url+'">click here</a> to go to site.<br/><br/>Thanks<br/>Forefront Team', email, 'Forefront Invitation')
        messages.success(self.request, 'Email Sent Successfully.')
        return redirect('invite')

    def get_test_func(self):
        return self.my_test_func

    def my_test_func(self):
        if self.request.user.is_authenticated and self.request.user.is_superuser:
            logout(self.request)
        return True    

# def demo_send_email(request):
#     send_demo_email(subject="demo email", message="Hi", from_email="sahiljain12345@gmail.com", to_email="sahiljain.1111.11@gmail.com")
#     # demosp2003@gmail.com
#     return JsonResponse({'success':'email sent'})

# def send_demo_email(subject, message, from_email=None, to_email=None):
#         if settings.MODE == "PROD":
#             from_email = 'sahiljain12345@gmail.com'
#             # client = boto3.client('ses', region_name=settings.REGION, aws_access_key_id=settings.AWS_ACCESS_KEY_ID, aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)
#             client = boto3.client('ses', region_name='us-east-2', aws_access_key_id='AKIAYVK6BQO554BESIX5', aws_secret_access_key='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')
#             response = client.send_email(
#                 Source=from_email,
#                 Destination={
#                     'ToAddresses': [
#                         to_email,
#                     ]
#                 },
#                 Message={
#                     'Subject': {
#                         'Data': subject,
#                         'Charset': 'UTF-8'
#                     },
#                     'Body': {
#                         'Html': {
#                             'Data': message,
#                             'Charset': 'UTF-8'
#                         }
#                     }
#                 }
#             )
#         else:
#             from_email = "demosp2003@gmail.com"
#             send_mail(subject, message, from_email, [to_email], html_message=message)        