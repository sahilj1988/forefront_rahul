import boto3
import os
from botocore.exceptions import ClientError
import logging
from django.conf import settings

class S3Handler:
    def __init__(self):
        # self.aws_access_key_id = os.environ.get('S3_UPLOADS_KEY')
        # self.aws_secret_access_key = os.environ.get('S3_UPLOADS_SECRET')
        # self.aws_region = os.environ.get('S3_UPLOADS_REGION')
        # self.bucket_name = os.environ.get('S3_UPLOADS_BUCKET')

        self.aws_access_key_id = settings.AWS_ACCESS_KEY_ID
        self.aws_secret_access_key = settings.AWS_SECRET_ACCESS_KEY
        self.aws_region = settings.AWS_S3_REGION
        self.bucket_name = settings.AWS_STORAGE_BUCKET_NAME

    def upload_file(self, file_path, file_name, s3_path):
        s3_client = boto3.client('s3', aws_access_key_id=self.aws_access_key_id,
                                 aws_secret_access_key=self.aws_secret_access_key, region_name=self.aws_region)
        try:
            # print(file_path)
            # print(self.bucket_name)
            # print(s3_path)
            s3_client.upload_file(file_path, self.bucket_name, s3_path, ExtraArgs={'ACL':'public-read'})
        except ClientError as e:
            logging.error(e)
            # print(e)